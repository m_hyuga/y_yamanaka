<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header(); ?>
	<section class="mainimg">
		<h1 class="headTitle"><img src="<?php bloginfo('template_url'); ?>/images/news/ttl.png" width="102" height="50" alt="ニュース NEWS"></h1>
	</section>

	<div id="contents">
		<ul class="path">
			<li><a href="<?php bloginfo('url'); ?>">ホーム</a>&#65310;</li>
			<li><a href="<?php bloginfo('url'); ?>/news/">ニュース</a>&#65310;</li>
			<li><?php single_tag_title(); ?></li>
		</ul>

		<section>
			<div class="inner clearfix">
				<div class="newsArea">
				<?php if (have_posts()) : 
					$news_cnt = 0;
					while (have_posts()) : the_post();
					$news_cnt++; ?>
					<h2 id="news_0<?php echo $news_cnt; ?>"><?php the_title(); ?></h2>
					<div class="cont fo15">
						<p class="dates fo13"><?php the_time('Y年m月d日'); ?></p>
						<div class="mceContentBody">
						<?php the_content(); ?>
						</div>
					</div>
					<div class="txt">
						<dl>
							<dt>カテゴリー：<?php $terms = get_the_terms( get_the_ID(), 'news_cat' );
																		if ( !empty($terms) ) {
																			if ( !is_wp_error( $terms ) ) {
																				foreach( $terms as $term ) {
																					echo $term->name;
																						if ($term !== end($terms)) {
																							echo '、';
																						}
																				}
																			}
																		} ?></dt>
						</dl>
					</div>
				<?php endwhile; endif; wp_reset_query();?>
				</div>

				<div class="sideNavi mb55">
					<ul class="stit">
						<li class="nobg"><img src="<?php bloginfo('template_url'); ?>/images/news/snavi_tit01.gif" width="200" height="38" alt="カテゴリー"></li>
					</ul>
					<ul>
						<?php wp_list_categories(array('title_li' => '', 'taxonomy' => 'news_cat')); ?>
					</ul>
				</div>

				<div class="sideNavi">
					<ul class="stit">
						<li class="nobg"><img src="<?php bloginfo('template_url'); ?>/images/news/snavi_tit02.gif" width="200" height="38" alt="過去のお知らせ"></li>
					</ul>
					<ul>
						<?php wp_get_archives('type=monthly&post_type=news&format=html'); ?>
					</ul>
				</div>
			</div>
			<div class="pager cf">
				<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
			</div>

		</section>

	</div><!-- //#content -->
<?php get_footer(); ?>
