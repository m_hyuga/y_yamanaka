<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
	<footer id="footer">
		<div id="finner">
			<ul class="fo11">
				<li><a href="<?php bloginfo('url'); ?>/concept/">コンセプト</a>|</li>
				<li><a href="<?php bloginfo('url'); ?>/about/">山中温泉について</a>|</li>
				<li><a href="<?php bloginfo('url'); ?>/hotel/">旅館紹介</a>|</li>
				<li><a href="<?php bloginfo('url'); ?>/form/">お問い合わせ・協会について</a>|</li>
				<li><a href="<?php bloginfo('url'); ?>/link/">リンク集</a>|</li>
				<li><a href="<?php bloginfo('url'); ?>/download/">パンフレットダウンロード</a>|</li>
			</ul>
			<p class="copyright"><small><img src="<?php bloginfo('template_url'); ?>/common/images/copyright02.png" width="294" height="12" alt="Copyright&copy;Yamanakaonsen All Right Reserved."></small></p>
		</div>
	</footer>
</div><!-- #wrapper -->
<?php wp_footer(); ?>
</body>
</html>