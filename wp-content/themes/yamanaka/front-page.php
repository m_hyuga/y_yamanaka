<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta name="keywords" content="石川,温泉,山中,観光,観光協会">
<meta name="description" content="山中温泉観光協会のサイトです。開湯1300年の歴史と豊かな自然、文人墨客が愛した情緒あふれる温泉地です。">
<?php wp_head(); ?>
<meta name="viewport" content="width=1400,initial-scale=0.25">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/common.css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/top.css">
<script src="<?php bloginfo('template_url'); ?>/common/js/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/common.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/js/colorbox/colorbox.css" />
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/colorbox/jquery.colorbox-min.js"></script>
<!--[if lt IE 9]>
<script src="<?php bloginfo('template_url'); ?>/common/js/html5shiv.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/css3-mediaqueries.js"></script>
<![endif]-->
<!-- <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/scrolltopcontrol.js"></script> -->

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/js/jquery.bxslider/jquery.bxslider.css">
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/skrollr-master/skrollr.min.js"></script>
<script type="text/javascript">
//スクロールゆらゆら定義
jQuery( function() {
	jQuery.fn.yurayura = function( config ){
		var obj = this;
		var i = 0;
		var defaults = {
			'move' : 5,			// 動く量
			'duration' : 1000,	// 移動にかける時間
			'delay' : 0			// 両端で停止する時間
		};
		var setting = jQuery.extend( defaults, config );
		( function move() {
			i = i > 0 ? -1 : 1;
			var p = obj.position().top;
			jQuery( obj )
				.delay( setting.delay )
				.animate( { top : p + i * setting.move }, {
					duration : setting.duration,
					complete : move
				});
		})();
	};
});

$(function(){
	//OP設定
	winHeight = $(window).height();  //ウィンドウの高さ  //グローバル化し、リサイズにも対応
var agent = navigator.userAgent;
if(agent.search(/iPhone/) != -1 || agent.search(/iPad/) != -1 || agent.search(/iPod/) != -1 || agent.search(/Android/) != -1){
	//spの場合
	$('#op').height(800);
	$('#op_slide').height(800);
	$('#op_slide li').height(800);
	$('.bx-viewport').height(800);
} else {
	//pcの場合
	$('#op').height(winHeight);
	$('#op_slide').height(winHeight);
	$('#op_slide li').height(winHeight);
	$('.bx-viewport').height(winHeight);
}
	//以下 オープニングスライド
	$(window).load(function(){
		$('#btn_scrl').delay(300).animate({'opacity':'1'}, 3000);
		$('.op_bottom,#op_slide').delay(300).fadeIn(3000, function(){
			$('.op_logo').delay(100).fadeIn(3000);
			//パララックス実行
			var agent = navigator.userAgent;
			if(agent.search(/iPhone/) != -1 || agent.search(/iPad/) != -1 || agent.search(/iPod/) != -1 || agent.search(/Android/) != -1){
				//spの場合
				return false;
			} else {
				//pcの場合
				var s = skrollr.init();
			}
		});
	});
	setTimeout(function(){
		opSlider();
	}, 5000);
	function opSlider() {
		$('#op_slide').bxSlider({
			mode: 'fade', //フェードで切替
			auto: true, //自動で切替
			speed: 2500, //移り変わるスピード
			pause: 4500, //スライドを見せる時間
			pager: false, //ページャー消す
			controls: false, //コントロール消す
			infiniteLoop: true //ループ無し
		});
	}
	//リサイズ時にも高さ調整
	$(window).resize(function(){
		//OP設定
		winHeight = $(window).height();  //ウィンドウの高さ
var agent = navigator.userAgent;
if(agent.search(/iPhone/) != -1 || agent.search(/iPad/) != -1 || agent.search(/iPod/) != -1 || agent.search(/Android/) != -1){
	//spの場合
	$('#op').height(800);
	$('#op_slide li').height(800);
	$('.bx-viewport').height(800);
} else {
	//pcの場合
	$('#op').height(winHeight);
	$('#op_slide li').height(winHeight);
	$('.bx-viewport').height(winHeight);
}
	});

	//pageTopボタン
	$(window).scroll(function(){
		if($(window).scrollTop() >= winHeight) {
			$('.slideWrap').css({'margin-top':'70px'});
			$('#topcontrol').fadeIn();
		} else {
			$('.slideWrap').css({'margin-top':'0'});
			$('#topcontrol').fadeOut();
		}
	});
	$('#topcontrol').click(function(){
		$('body, html').animate({ scrollTop: 0 }, 500);
	});
		var slider = $('#slides').bxSlider({
			auto: true,
			slideWidth: 980,
			speed: 1500, //移り変わるスピード
			pause: 5500, //スライドを見せる時間
			pager: false,
			minSlides: 3,
			maxSlides: 3,
			moveSlides: 1,
			slideMargin: 0,
			onSlideBefore: function(){
				slider.startAuto();
			},
			onSlideAfter: function(){
				slider.startAuto();
			}
		});

	//ループバナー
	var loopSlider = $('.loopslider').bxSlider({
		auto: true,
		autoStart: true,
		infiniteLoop: true,
		moveSlides: 3,
		slideWidth: 317,
		randomStart: false,
		minSlides: 3,
		maxSlides: 12,
		slideMargin: 15,
		speed: 2500,
		pause: 5000,
		pager: false,
		controls: true,
		onSliderLoad: function() {
			loopControls();
		},
		onSlideAfter: function(){
			loopSlider.startAuto();
		}
	});
	function loopControls() {
		//ループスライダーのコントロール制御
			//前へボタン
		$(document).on('click', '.calWrap .slideBefore', function(){
			$('#loopBannerArea .bx-wrapper .bx-prev').trigger('click');
		});
			//次へボタン
		$(document).on('click', '.calWrap .slideAfter', function(){
			$('#loopBannerArea .bx-wrapper .bx-next').trigger('click');
		});
	}

	//カルーセル
	var car2 = $('#pla_cal').bxSlider({
		ticker: true,
		slideWidth: 250,
		speed: 70000, //移り変わるスピード
		pager: false,
		controls: false,
		slideMargin: 15
	});



var agent = navigator.userAgent;
if(agent.search(/iPhone/) != -1 || agent.search(/iPad/) != -1 || agent.search(/iPod/) != -1 || agent.search(/Android/) != -1){
	//spの場合
} else {
	//pcの場合
	//メニュー追従
	$(window).scroll(function(){
		if($(window).scrollTop() >= winHeight) {
			$('#header').css({
				'position':'fixed',
				'top':0,
				'left':0
			});
		} else {
			$('#header').css({
				'position':'relative'
			});
		}
	});
}
	//ゆらゆら実行
	$('#btn_scrl').yurayura({
		'move' : 20, //動く量
		'delay' : 10, //両端で停止する時間
		'duration' : 600 //移動にかける時間
	});
	//スクロールボタン
	$('#btn_scrl').click(function(){
		$('body, html').animate({ scrollTop: winHeight }, 500);
	});

	//魅力のオーバ処理
	$('.conceptTop .bnr li').hover(function(){
		$(this).children('img').stop(true,false).animate({
			'width':545,
			'height':252,
			'top':-16,
			'left':-35
		}, 250);
	}, function(){
		$(this).children('img').stop(true,false).animate({
			'width':475,
			'height':220,
			'top':0,
			'left':0
		}, 250);
	});


});
</script>
</head>

<body id="topIndex">
<div id="wrapper">
	<div id="op" class="bgWh">
		<div id="btn_scrl"><img src="<?php bloginfo('template_url'); ?>/images/top/btn_scrlv2.png" alt="スクロール"></div>
		<div class="op_bottom"><img src="<?php bloginfo('template_url'); ?>/images/top/op/op_bottomv2.png" width="100%" alt=""></div>
		<div class="op_logo"><img src="<?php bloginfo('template_url'); ?>/images/top/op/slide_imgv2.png" width="270" height="387" alt="日本の心に、つかろう。"></div>
			<ul id="op_slide" data-top-top="top:0%;" data-top-bottom="top:20%;">
				<li class="slide1v2"></li>
				<li class="slide2v2"></li>
				<li class="slide3v2"></li>
				<li class="slide4v2"></li>
			</ul>
	</div>

	<header id="header">
		<div class="inner">
			<h1 id="logo"><img src="<?php bloginfo('template_url'); ?>/common/images/logo.png" width="122" height="42" alt="日本の美と、渓谷の温泉と。山中温泉"></h1>
			<dl class="hnavi">
				<dt><a href="http://www.yamanaka-spa.or.jp/global/eng/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/english_off.png" width="58" height="20" alt="ENGLISH"></a></dt>
				<dd><a href="https://www.facebook.com/pages/%E5%B1%B1%E4%B8%AD%E6%B8%A9%E6%B3%89%E8%A6%B3%E5%85%89%E5%8D%94%E4%BC%9A/251607508211337" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/btn_facebook_off.png" width="24" height="24" alt="facebook"></a></dd>
				<!--<dd><a href="#" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/btn_twitter_off.png" width="23" height="24" alt="twiiter"></a></dd>-->
			</dl>
			<ul id="navi">
				<li><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn01_on.png" width="41" height="13" alt="ホーム"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/concept/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn02_off.png" width="68" height="13" alt="コンセプト"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/about/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn03_off.png" width="121" height="13" alt="山中温泉について"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/hotel/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn04_off.png" width="63" height="13" alt="旅館紹介"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/highlights/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn05_off.png" width="56" height="13" alt="見どころ"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/event/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn06_off.png" width="53" height="13" alt="イベント情報"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/access/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn07_off.png" width="53" height="13" alt="アクセス情報"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/form/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn08_off.png" width="94" height="13" alt="お問い合わせ"></a></li>
			</ul>
		</div>
	</header><!-- //#header -->

<div class="slideBg">
	<div class="slideWrap">
		<div class="slideWrap_inner">
			<div class="flexslider2">
				<ul id="slides">
				<?php
				if (have_posts()) : 
				while (have_posts()) : the_post();
				$repeat_group = scf::get('slide_area');
				foreach ( $repeat_group as $field_name => $field_value ) :
					$txt_link = $field_value['txt_link'];
					$val =  $field_value["slide_img"];
					echo '<li>';
					if (!empty($txt_link)) { echo '<a href="'.$txt_link.'">';}
					$image = wp_get_attachment_image_src($val, 'full');
					echo '<img src="'.$image[0].'">';
					if (!empty($txt_link)) { echo '</a>';}
					echo '</li>';
					
				 endforeach;
				 endwhile; endif; ?>
				</ul>
			</div>
		</div>
	</div>
	<div class="calWrap">
		<div class="calWrap_inner">
			<div id="loopBannerArea">
				<ul class="loopslider">
				<?php
				if (have_posts()) : 
				while (have_posts()) : the_post();
				$repeat_group = scf::get('slide_area_mini');
				foreach ( $repeat_group as $field_name => $field_value ) :
					$txt_link = $field_value['txt_mini_link'];
					$txt_txt = $field_value['txt_mini_txt'];
					$val =  $field_value["slide_mini_img"];
					$check_mini_check = $field_value['check_mini_check'];
					$youtube_url = $field_value['check_mini_url'];
					echo '<li>';
					if (!empty($txt_link)) { echo '<a href="'.$txt_link.'">';}
					if ($check_mini_check == '使う') {echo '<a href="https://www.youtube.com/embed/'.$youtube_url.'?rel=0" class="colorbox_youtube">';}
					$image = wp_get_attachment_image_src($val, 'full');
					echo '<img src="'.$image[0].'"><span>'.$txt_txt.'</span>';
					if (!empty($txt_link) or $check_mini_check == '使う') { echo '</a>';}
					echo '</li>';
					
				 endforeach;
				 endwhile; endif;wp_reset_postdata(); ?>
				</ul>
			</div>
		</div>
		<span class="slideBefore"></span>
		<span class="slideAfter"></span>
	</div>
</div>

	<div id="contents">

		<section class="conceptTop">
			<div class="inner">
				<h2 class="alignCenter"><img src="<?php bloginfo('template_url'); ?>/images/top/ttl01.png" alt="山中温泉の魅力"></h2>
				<p class="appTxt fo14 txt lheight">開湯1300年、山中温泉は豊かな自然と伝統文化が残る渓谷の温泉地です。<br>松尾芭蕉も愛してやまなかった美しい湯のまちで｢日本の心｣につかりませんか。</p>
				<ul class="bnr cf">
					<li><img src="<?php bloginfo('template_url'); ?>/images/top/app_01.jpg" class="bgov" alt=""><a href="<?php bloginfo('url'); ?>/about/"><img src="<?php bloginfo('template_url'); ?>/images/top/app_01_txt.png" alt=""></a></li>
					<li><img src="<?php bloginfo('template_url'); ?>/images/top/app_02.jpg" class="bgov" alt=""><a href="<?php bloginfo('url'); ?>/hotel/"><img src="<?php bloginfo('template_url'); ?>/images/top/app_02_txt.png" alt=""></a></li>
					<li><img src="<?php bloginfo('template_url'); ?>/images/top/app_03.jpg" class="bgov" alt=""><a href="<?php bloginfo('url'); ?>/highlights/"><img src="<?php bloginfo('template_url'); ?>/images/top/app_03_txt.png" alt=""></a></li>
					<li><img src="<?php bloginfo('template_url'); ?>/images/top/app_04.jpg" class="bgov" alt=""><a href="<?php bloginfo('url'); ?>/event/"><img src="<?php bloginfo('template_url'); ?>/images/top/app_04_txt.png" alt=""></a></li>
				</ul>
			</div>
		</section>


		<section class="pickUp">
			<div class="inner">
				<h2 class="alignCenter"><img src="<?php bloginfo('template_url'); ?>/images/top/ttl02.png" alt="ピックアップ"></h2>
				<p class="pickTxt fo14 txt lheight">山中温泉の数ある魅力の中から、おすすめの情報をピックアップ。<br>訪れるたびに新しい出会いがあるのも、山中温泉の楽しみです。</p>
				<ul class="picklist cf">
				<?php
				query_posts(
					array(
					'post_type' => 'page',
					'page_id' => 227
					 ) 
				);
				// ループ
				if ( have_posts() ) : 
					while ( have_posts() ) : the_post();
				$pickuplist = array(1, 2, 3);
				foreach ($pickuplist as $pickup){
						$image_id = SCF::get('pickup_img_0'.$pickup);$image = wp_get_attachment_image_src($image_id, 'full');
						$pickup_title = SCF::get( 'pickup_title_0'.$pickup );
						$pickup_txt = SCF::get( 'pickup_txt_0'.$pickup );
						$pickup_url = SCF::get( 'pickup_url_0'.$pickup );
						if ($image[0]!="" or $pickup_title!="" ) { 
						echo '<li class="ic_pick"><a href="'.$pickup_url.'">';
						if ( $image[0] != ''){
								echo '<img src="'.$image[0].'" alt="">';
							}
						echo '<h4>'.esc_html( $pickup_title ).'</h4>
							<p>'.nl2br(esc_html( $pickup_txt )).'</p>
						</a></li>
						';
						}
				}
					endwhile; 
				endif;
				
				// クエリをリセット
				wp_reset_query();
			 ?>
				</ul>
			</div>
		</section><!-- /pickUp -->

		<section class="pla">
			<h2 class="alignCenter"><img src="<?php bloginfo('template_url'); ?>/images/top/ttl03.png" alt="見どころ紹介"></h2>
			<p class="plaTxt fo14 txt lheight">歴史あるものも、新しいものも。豊かな文化も、美しい自然も。<br>ここにしかない多彩な見どころを、ぜひ体感してください。</p>
			<div class="pla_calWrap">
				<div class="pla_calInner">
					<div class="pla_calmr">
						<ul id="pla_cal">
						<?php the_content(); ?>
						</ul>
					</div>
				</div>
			</div>
			<div class="list_btn"><a href="<?php bloginfo('url'); ?>/highlights/landmark/">一覧はこちら</a></div>
		</section><!-- /pla -->



		<section class="newsTop">
			<div class="inner">
				<h2 class="alignCenter pb25"><img src="<?php bloginfo('template_url'); ?>/images/top/ttl04.png" alt="ニュース NEWS"></h2>
				<div class="news">
					<ul class="fo14">

					<?php
					$news_cnt = 0;
					query_posts(
						array(
						'post_type' => 'news',
						'posts_per_page' => 3
						 ) 
					);
					if (have_posts()) : while (have_posts()) : the_post(); 
					$news_cnt++;
					?>
						<li<?php if(is_last_post()){ echo ' class="noln"';}; ?>>
							<p class="ico"><img src="<?php bloginfo('template_url'); ?>/images/top/ico_news.gif" width="62" height="22" alt="NEWS"></p>
							<p class="dates"><?php the_time('Y.m.d'); ?></p>
							<p class="txt"><a href="<?php echo get_the_permalink(); ?>"><?php if(mb_strlen($post->post_title)>35) { $title= mb_substr($post->post_title,0,35) ; echo $title. ･･･ ;} else {echo $post->post_title;}?></a></p>
						</li>
					<?php endwhile; endif; ?>
					</ul>
				</div>
			<p class="linkBtn fo11"><a href="<?php bloginfo('url'); ?>/news/">一覧はこちら</a></p>
			</div>
		</section>
</div><!-- //#content -->

<footer id="footer">
	<div id="finner">
		<ul class="fo11">
			<li><a href="<?php bloginfo('url'); ?>/concept/">コンセプト</a>|</li>
			<li><a href="<?php bloginfo('url'); ?>/about/">山中温泉について</a>|</li>
			<li><a href="<?php bloginfo('url'); ?>/hotel/">旅館紹介</a>|</li>
			<li><a href="<?php bloginfo('url'); ?>/form/">お問い合わせ・協会について</a>|</li>
			<li><a href="<?php bloginfo('url'); ?>/link/">リンク集</a>|</li>
			<li><a href="<?php bloginfo('url'); ?>/download/">パンフレットダウンロード</a>|</li>
		</ul>
		<p class="copyright"><small><img src="<?php bloginfo('template_url'); ?>/common/images/copyright.png" width="294" height="12" alt="Copyright&copy;Yamanakaonsen All Right Reserved."></small></p>
	</div>
</footer>
</div><!-- #wrapper -->
<div id="topcontrol">
<div id="scrolltopcontrol"></div>
</div>
<?php wp_footer(); ?>
</body>
</html>
