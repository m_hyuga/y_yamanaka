<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
global $post;
?>
<!DOCTYPE html>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta name="keywords" content="石川,温泉,山中,観光,観光協会">
<meta name="description" content="山中温泉観光協会のサイトです。開湯1300年の歴史と豊かな自然、文人墨客が愛した情緒あふれる温泉地です。">
<?php wp_head(); ?>
<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> -->
<meta name="viewport" content="width=1400,initial-scale=0.25">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/common.css">
<script src="<?php bloginfo('template_url'); ?>/common/js/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/common.js"></script>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/js/colorbox/colorbox.css" />
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/colorbox/jquery.colorbox-min.js"></script>
<?php if (is_post_type_archive('eventdetail') or is_post_type_archive('event') or is_singular('eventdetail') or is_singular('event')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/event.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/editor-style.css" />
<?php
endif; 
if (is_post_type_archive('news') or is_tax('news_cat') or is_singular('news')) : ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/news.css">
	<script>
	$(function(){$(".colorbox").colorbox();});
	</script>
<?php
endif; 
if (is_page('about')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/about.css">
<?php
endif; 
if (is_page('access')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/access.css">
	<script src="https://maps.google.com/maps/api/js"></script>
<script>
/* DOMの読み込み完了後に処理 */
google.maps.event.addDomListener( window , 'load', googleMapsSyncer );

//地図のインスタンスを格納する変数
var map = null ;

//マーカーのインスタンスを格納する変数
var markers = [];

//Google Mapsのプログラムを実行する
function googleMapsSyncer(){

//キャンパスの要素を取得する
var canvas = document.getElementById( "map-canvas" );

//中心の位置座標を指定する
var latlng = new google.maps.LatLng( 36.251946 , 136.374303 );

//地図のオプションを設定する
var mapOptions = {
zoom: 15 ,				//ズーム値
center: latlng ,		//中心座標 [latlng]
scrollwheel: false
};

//[canvas]に、[mapOptions]の内容の、地図のインスタンス([map])を作成する
map = new google.maps.Map( canvas , mapOptions );

//マーカーを作成、設置する
//markers[1]、markers[2]と増やしていくと、後々の管理が楽になります。
markers[0] = new google.maps.Marker( { map: map , position: latlng , icon:'../common/images/common/icon/map_marker.png' } );

}
</script>
<script>
$(function(){
	$('a.kakudai').colorbox();
});
</script>
<?php
endif; 
if (is_page('concept')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/concept.css">
<?php
endif; 
if (is_page('download')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/download.css">
<?php
endif; 
if (is_page('form')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/form.css">
	<script src="https://maps.google.com/maps/api/js"></script>
	<script>
	/* DOMの読み込み完了後に処理 */
	google.maps.event.addDomListener( window , 'load', googleMapsSyncer );
	
	//地図のインスタンスを格納する変数
	var map = null ;
	
	//マーカーのインスタンスを格納する変数
	var markers = [];
	
	//Google Mapsのプログラムを実行する
	function googleMapsSyncer(){
	
	//キャンパスの要素を取得する
	var canvas = document.getElementById( "map-canvas" );
	
	//中心の位置座標を指定する
	var latlng = new google.maps.LatLng( 36.251946 , 136.374303 );
	
	//地図のオプションを設定する
	var mapOptions = {
	zoom: 15 ,				//ズーム値
	center: latlng ,		//中心座標 [latlng]
	scrollwheel: false
	};
	
	//[canvas]に、[mapOptions]の内容の、地図のインスタンス([map])を作成する
	map = new google.maps.Map( canvas , mapOptions );
	
	//マーカーを作成、設置する
	//markers[1]、markers[2]と増やしていくと、後々の管理が楽になります。
	markers[0] = new google.maps.Marker( { map: map , position: latlng , icon:'<?php bloginfo('template_url'); ?>/common/images/common/icon/map_marker.png' } );
	
	}
	</script>
<?php
endif; 
if (is_page('highlights') || is_parent_slug() === 'highlights' || is_parent_slug() === 'walk' || is_parent_slug() === 'map'): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/highlights.css">
<script language="JavaScript">
$(function(){
	$(".inline").colorbox({
		inline:true,
		width:980,
		height:550
	});
});
</script>
<script>
$(function(){
	$('area').on('click', function(){
		var elm = $(this).attr('data-class'); //クリックされたareaタグの属性を取得
		var offsetTop = $('#'+elm).offset().top; //クリックされたボタンと同じIDを持つ要素のTOP位置を取得
		var trg = offsetTop - 90; //要素のTOP位置から−90pxする(ヘッダーなどの事を考慮)
		$('body, html').animate({scrollTop: trg}, 500); //0.5秒かけて、それぞれに移動
	});
});
</script>
<?php
endif; 
if (is_page('map')): ?>
	<script src="https://maps.google.com/maps/api/js"></script>
	<script src="<?php bloginfo('template_url'); ?>/common/js/map.js"></script>
<?php
endif; 
if (is_page('highlights')): ?>
	<script src="https://maps.google.com/maps/api/js"></script>
	<script src="<?php bloginfo('template_url'); ?>/common/js/map.js"></script>
<script>
$(function(){
$('.picList li').hover(function(){
	$(this).children('img').stop(true,false).animate({
		'top':-18,
		'left':-36,
		'width':552,
		'height':287
	}, 250);
}, function(){
	$(this).children('img').stop(true,false).animate({
		'top':0,
		'left':0,
		'width':480,
		'height':250
	}, 250);
});
});
</script>

<?php
endif; 
if (is_page('circumference')): ?>
	<script src="https://maps.google.com/maps/api/js"></script>
	<script src="<?php bloginfo('template_url'); ?>/common/js/map2.js"></script>
<?php
endif; 
if (is_page('landmark')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/highlights.css">
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/jquery.tile.min.js"></script>
<?php
endif; 
if (is_page('hotel')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/hotel.css">
<script>
$(function () {
    var columns = 2;
    var liLen = $('.list_area li').length;
    var lineLen = Math.ceil(liLen / columns);
    var liHiArr = [];
    var lineHiArr = [];

    for (i = 0; i <= liLen; i++) {
        liHiArr[i] = $('.list_area li').eq(i).height();
        if (lineHiArr.length <= Math.floor(i / columns) 
            || lineHiArr[Math.floor(i / columns)] < liHiArr[i]) 
        {
            lineHiArr[Math.floor(i / columns)] = liHiArr[i];
        }
    }
    $('.list_area li').each(function(i){
        $(this).css('height',lineHiArr[Math.floor(i / columns)] + 'px');
    });
		
});
</script>
<?php
endif; 
if (is_page('link')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/link.css">
<?php
endif; 
if (is_page('route')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/route.css">
<?php endif; ?>
<!--[if lt IE 9]>
<script src="<?php bloginfo('template_url'); ?>/common/js/html5shiv.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/css3-mediaqueries.js"></script>
<![endif]-->
<!-- googleアクセス解析 --> 
</script>
<script type="text/javascript">
_uacct = "UA-928173-9";
urchinTracker();
swfobject.registerObject("FlashID");
</script>
<!-- /googleアクセス解析 --> 

</head>

<body <?php body_class(); ?>>
<div id="wrapper">
<header id="header">
<div class="inner">
	<p id="logo"><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/common/images/logo.png" alt="日本の美と、渓谷の温泉と。山中温泉"></a></p>
	<dl class="hnavi">
		<dt><a href="https://www.yamanaka-spa.or.jp/global/eng/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/english_off.png" alt="ENGLISH"></a></dt>
		<dd><a href="https://www.facebook.com/pages/%E5%B1%B1%E4%B8%AD%E6%B8%A9%E6%B3%89%E8%A6%B3%E5%85%89%E5%8D%94%E4%BC%9A/251607508211337" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/btn_facebook_off.png" alt="facebook"></a></dd>
		<!--<dd><a href="#" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/btn_twitter_off.png" width="23" height="24" alt="twiiter"></a></dd>-->
	</dl>
	<ul id="navi">
		<li><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn01_off.png" alt="ホーム"></a></li>
		<li><a href="<?php bloginfo('url'); ?>/concept/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn02_<?php if (is_page('concept')){ echo 'on';}else{echo 'off';} ?>.png" alt="コンセプト"></a></li>
		<li><a href="<?php bloginfo('url'); ?>/about/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn03_<?php if (is_page('about')){ echo 'on';}else{echo 'off';} ?>.png" alt="山中温泉について"></a></li>
		<li><a href="<?php bloginfo('url'); ?>/hotel/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn04_<?php if (is_page('hotel')){ echo 'on';}else{echo 'off';} ?>.png" alt="旅館紹介"></a></li>
		<li><a href="<?php bloginfo('url'); ?>/highlights/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn05_<?php if (is_page('highlights') || is_parent_slug() === 'highlights' || is_parent_slug() === 'walk' || is_parent_slug() === 'map'){ echo 'on';}else{echo 'off';} ?>.png" alt="見どころ"></a></li>
		<li><a href="<?php bloginfo('url'); ?>/event/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn06_<?php if (is_post_type_archive('eventdetail') or is_post_type_archive('event') or is_singular('eventdetail') or is_singular('event')){ echo 'on';}else{echo 'off';} ?>.png" alt="イベント情報"></a></li>
		<li><a href="<?php bloginfo('url'); ?>/access/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn07_<?php if (is_page('access')){ echo 'on';}else{echo 'off';} ?>.png" alt="アクセス情報"></a></li>
		<li><a href="<?php bloginfo('url'); ?>/form/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn08_<?php if (is_page('form')){ echo 'on';}else{echo 'off';} ?>.png" alt="お問い合わせ"></a></li>
	</ul>
</div>
</header><!-- //#header -->
