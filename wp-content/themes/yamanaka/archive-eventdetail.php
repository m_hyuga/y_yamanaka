<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header(); ?>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/scrolltopcontrol.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/jquery.tile.min.js"></script>
<script>
$(function () {
    var columns = 4;
    var liLen = $('.eventList #list .date').length;
    var lineLen = Math.ceil(liLen / columns);
    var liHiArr = [];
    var lineHiArr = [];

    for (i = 0; i <= liLen; i++) {
        liHiArr[i] = $('.eventList #list .date').eq(i).height();
        if (lineHiArr.length <= Math.floor(i / columns) 
            || lineHiArr[Math.floor(i / columns)] < liHiArr[i]) 
        {
            lineHiArr[Math.floor(i / columns)] = liHiArr[i];
        }
    }
    $('.eventList #list .date').each(function(i){
        $(this).css('height',lineHiArr[Math.floor(i / columns)] + 'px');
    });

});
</script>
<script type="text/javascript">
$(function(){
	$('.tile').tile();
	$('.name').tile(4);
	$('#list li:nth-child(4n+1)').css({'margin-left':'0'});
});
</script>
<section class="mainimg">
<h1 class="headTitle"><img src="<?php bloginfo('template_url'); ?>/images/event/ttl.png" width="98" height="42" alt="イベント EVENT"></h1>
</section>

<div id="contents">
<ul class="path">
<li><a href="<?php bloginfo('url'); ?>">ホーム</a>&#65310;</li>
<li><a href="<?php bloginfo('url'); ?>/event/">イベント情報</a>&#65310;</li>
<li>新着イベント情報一覧</li>
</ul>

<section>
<div class="inner">
	<div class="eventList clearfix">
		<h2 class="pb30"><img src="<?php bloginfo('template_url'); ?>/images/event/list/title_list.png" width="980" height="40" alt="新着イベント情報一覧"></h2>
		<ul id="list" class="clearfix">
		<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<li>
			<?php 
				$args = array(
								'connected_type' => 'posts_to_events',
								'connected_items' =>  $post->ID,
								'nopaging' => true,
								'suppress_filters' => false
				);
				$connected_posts = get_posts( $args );
				foreach ( $connected_posts as $post ) {
				setup_postdata( $post ); 
				// 画像
				$image_id = SCF::get('img_main');
				$image = wp_get_attachment_image_src($image_id, 'full');
				if (!empty($image_id)) {
					echo '<p class="thumb" style="background-image: url(\''.$image[0].'\')"></p>';
				};
				
				// タイトル
				$title = get_the_title();
				echo '<p class="name">'.$title.'</p>';
				
				// リンク
				$parm = get_permalink();
	
				} wp_reset_postdata();?>
			<div class="date">
				<?php
				$txt_date =  scf::get('txt_date');
				$txt_time =  scf::get('txt_time');
				$txt_area =  scf::get('txt_area');
				
				if (!empty($txt_date)) {
					echo '<dl><dt>&#12296;日 時&#12297;</dt><dd>';
					$date_ver = strip_tags(get_post_meta($post->ID, 'txt_date', true));
					echo mb_substr($date_ver,0,40);
					if ( mb_strlen($date_ver) >= 41 ){ echo '...';};
					echo '</dd></dl>';
				}
				if (!empty($txt_area)){
					echo '<dl><dt>&#12296;場 所&#12297;</dt><dd>';
					echo nl2br(get_post_meta($post->ID, 'txt_area', true));
					echo '</dd></dl>';
				}
				?>
			</div>
				<p class="btn"><a href="<?php echo $parm; ?>"><img src="<?php bloginfo('template_url'); ?>/images/event/btn01.gif" alt="詳細を見る"></a></p>
			</li>
		<?php endwhile; endif; ?>
		</ul>
		
		<div class="pager cf">
			<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
		</div>
	</div>
</div>
</section>

</div><!-- //#content -->
<?php get_footer(); ?>
