<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header(); ?>
<script>
$(function () {
    var columns = 3;
    var liLen = $('.event_new .tit').length;
    var lineLen = Math.ceil(liLen / columns);
    var liHiArr = [];
    var lineHiArr = [];

    for (i = 0; i <= liLen; i++) {
        liHiArr[i] = $('.event_new .tit').eq(i).height();
        if (lineHiArr.length <= Math.floor(i / columns) 
            || lineHiArr[Math.floor(i / columns)] < liHiArr[i]) 
        {
            lineHiArr[Math.floor(i / columns)] = liHiArr[i];
        }
    }
    $('.event_new .tit').each(function(i){
        $(this).css('height',lineHiArr[Math.floor(i / columns)] + 'px');
    });
		
    var columns = 3;
    var liLen = $('.event_new .date').length;
    var lineLen = Math.ceil(liLen / columns);
    var liHiArr = [];
    var lineHiArr = [];

    for (i = 0; i <= liLen; i++) {
        liHiArr[i] = $('.event_new .date').eq(i).height();
        if (lineHiArr.length <= Math.floor(i / columns) 
            || lineHiArr[Math.floor(i / columns)] < liHiArr[i]) 
        {
            lineHiArr[Math.floor(i / columns)] = liHiArr[i];
        }
    }
    $('.event_new .date').each(function(i){
        $(this).css('height',lineHiArr[Math.floor(i / columns)] + 'px');
    });

});
</script>
<section class="mainimg">
<h1 class="headTitle"><img src="<?php bloginfo('template_url'); ?>/images/event/ttl.png" width="98" height="42" alt="イベント EVENT"></h1>
</section>

<div id="contents">
<ul class="path">
	<li><a href="<?php bloginfo('url'); ?>">ホーム</a>&#65310;</li>
	<li>イベント</li>
</ul>

<section>
	<div class="inner">
		<div class="eventArea clearfix">
		<p class="txt fo18">山中温泉では、四季折々のまつりやイベントが開催されています。<br>
		人と自然、伝統と文化にふれるひとときをお楽しみください。</p>
		<h2 class="pb30"><img src="<?php bloginfo('template_url'); ?>/images/event/title01.png" width="980" height="40" alt="新着イベント情報"></h2>
		<p class="fo18 pb30">現在開催中・近日開催予定のイベントをご案内いたします。</p>

		<div class="event_new">
		<?php 
		query_posts(
			array(
			'post_type' => 'eventdetail',
			'posts_per_page' => 3
			 ) 
		);
		$for_cnt = 0;
		if(have_posts()): while(have_posts()): the_post(); ?>
		<div class="cont<?php $for_cnt++;	if ( $for_cnt == 3 ) {	echo ' pr00';} ?>">
			<?php 
			$args = array(
							'connected_type' => 'posts_to_events',
							'connected_items' =>  $post->ID,
							'nopaging' => true,
							'suppress_filters' => false
			);
			$connected_posts = get_posts( $args );
			foreach ( $connected_posts as $post ) {
			setup_postdata( $post ); 
			// 画像
			$image_id = SCF::get('img_main');
			$image = wp_get_attachment_image_src($image_id, 'full');
			if (!empty($image_id)) {
				echo '<p class="thumb" style="background-image: url(\''.$image[0].'\')"></p>';
			};
			
			// タイトル
			$title = get_the_title();
			echo '<p class="tit fo20">'.$title.'</p>';
			
			// リンク
			$parm = get_permalink();

			} wp_reset_postdata();?>
			<div class="date">
				<?php
				$txt_date =  scf::get('txt_date');
				$txt_time =  scf::get('txt_time');
				$txt_area =  scf::get('txt_area');
				
				if (!empty($txt_date)) {
					echo '<dl><dt>&#12296;日 時&#12297;</dt><dd>';
					$date_ver = strip_tags(get_post_meta($post->ID, 'txt_date', true));
					echo mb_substr($date_ver,0,40);
					if ( mb_strlen($date_ver) >= 41 ){ echo '...';};
					echo '</dd></dl>';
				}
				if (!empty($txt_time)){
					echo '<dl><dt>&#12296;時 間&#12297;</dt><dd>';
					echo nl2br(get_post_meta($post->ID, 'txt_time', true));
					echo '</dd></dl>';
				}
				if (!empty($txt_area)){
					echo '<dl><dt>&#12296;場 所&#12297;</dt><dd>';
					echo nl2br(get_post_meta($post->ID, 'txt_area', true));
					echo '</dd></dl>';
				}
				?>
				<p class="ico"><img src="<?php bloginfo('template_url'); ?>/images/event/ico_new.png" width="75" height="75"></p>
			</div>
			<p class="alignCenter"><a href="<?php echo $parm; ?>" class="al"><img src="<?php bloginfo('template_url'); ?>/images/event/btn01.gif" width="144" height="21" alt="詳細を見る"></a></p>
		</div>
		<?php endwhile; endif; wp_reset_query(); ?>
		</div>
		
		<p class="linkBtn fo11 mb70"><a href="<?php bloginfo('url'); ?>/eventdetail/">一覧はこちら</a></p>
		
		<h2 class="pb30"><img src="<?php bloginfo('template_url'); ?>/images/event/title02.png" width="980" height="40" alt="年間イベント情報"></h2>
		<p class="fo18 pb30">山中温泉では年間を通じて数多くのイベントを開催しています。</p>
		<ul class="listEvent clearfix">
		<?php
		$seasonlist = array('ico01','ico02','ico03','ico04');
		$event_cnt = 0;
		foreach ($seasonlist as $season){
			if ($season == 'ico01'){
				//春
				$eventllist = array(104,113,114);
			}
			if ($season == 'ico02'){
				//夏
				$eventllist = array(116,118,119);
			}
			if ($season == 'ico03'){
				//秋
				$eventllist = array(120,121,122,123,124);
			}
			if ($season == 'ico04'){
				//冬
				$eventllist = array(110,125);
			}
			foreach ($eventllist as $event){
				$posts = get_posts(array("include"=>$event, "post_type"=>'event')); global $post;
				if($posts): foreach($posts as $post): setup_postdata($post); $id = $post->ID;
					$event_cnt++;
					if (($event_cnt >= 2) and ($event_cnt % 4) == 1) { echo '<ul class="listEvent clearfix">';};
					if (($event_cnt >= 2) and (($event_cnt % 4) == 0)) { echo '<li class="mr00">';} else { echo '<li>';};
					$image_id = SCF::get('img_main');
					$image = wp_get_attachment_image_src($image_id, 'full');	
					echo '<p><img src="'.$image[0].'" width="223" height="171"></p>';
					echo '<p class="tit fo15">'.nl2br(get_post_meta($post->ID, 'txt_list', true)).'</p>';
					echo '<p class="alignCenter"><a href="'.get_the_permalink().'" class="al"><img src="'.get_template_directory_uri().'/images/event/btn01.gif" width="144" height="21" alt="詳細を見る"></a></p>';
					echo '<p class="ico"><img src="'.get_template_directory_uri().'/images/event/'.$season.'.png" width="60" height="60"></p>';
					echo '</li>';
					if (($event_cnt >= 2) and (($event_cnt % 4) == 0)) { echo '</ul>';};
				endforeach; endif; wp_reset_postdata();
				}
			}
		 ?>
		 </ul>
		</div>
	</div>
</section>

</div><!-- //#content -->

<?php get_footer(); ?>
