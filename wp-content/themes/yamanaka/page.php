<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header();
global $post;
if('19' == $post->post_parent): ?>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/scrolltopcontrol.js"></script>
<script type="text/javascript">
$(window).load(function(){
	var contHeight = $('.cont02').height();
	$('.eventDeta').height(contHeight-53);
});
</script>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<section class="mainimg">
<h1 class="headTitle"><img src="<?php bloginfo('template_url'); ?>/images/event/ttl.png" width="166" height="50" alt="イベント情報 EVENT INFORMATION"></h1>
</section>

<div id="contents">
<ul class="path">
	<li><a href="<?php bloginfo('url'); ?>">ホーム</a>&#65310;</li>
	<li><a href="<?php bloginfo('url'); ?>/event/">イベント情報</a>&#65310;</li>
	<li><?php the_title(); ?></li>
</ul>

<section>
	<div class="inner">
		<div class="pageArea">
			<div class="cont clearfix">
				<?php
				$image_id = SCF::get('img_main');
				$image = wp_get_attachment_image_src($image_id, 'full');
				if (!empty($image_id)) {
					echo '<p class="img"><img src="'.$image[0].'"></p>';
				};
				?>
				<h2><?php the_title(); ?></h2>
				<div>
					<?php
					$val = nl2br(get_post_meta($post->ID, 'txt_big', true));
					if (!empty($val)){
						echo '<p class="tit fo18">'.$val.'</p>';
					}
					$val = nl2br(get_post_meta($post->ID, 'txt_main', true));
					if (!empty($val)){
						echo '<p class="fo15">'.$val.'</p>';
					}
					?>
				</div>
			</div>

			<div class="cont02 clearfix">
				<div class="detaImg">
					<p class="img"><img src="<?php bloginfo('template_url'); ?>/images/event/page/01/img02.jpg" alt="大鍋の振る舞い「かに汁大鍋」"></p>
				</div>
				<div class="eventDeta fo14">
					<h3><img src="<?php bloginfo('template_url'); ?>/images/event/page/event_deta.png" width="136" height="25" alt=""></h3>
					<dl>
						<dt>&#12296;期 間&#12297;</dt>
						<dd>2015年11月1日(日)〜2016年3月31日(木)</dd>
					</dl>
					<dl>
						<dt>&#12296;時 間&#12297;</dt>
						<dd>10:00〜11:30</dd>
					</dl>
					<dl>
						<dt>&#12296;料 金&#12297;</dt>
						<dd>1杯200円</dd>
					</dl>
					<dl>
						<dt>&#12296;場 所&#12297;</dt>
						<dd>町人旅人亭(ゆげ街道沿い)</dd>
					</dl>
					<p><span class="pr20">&#12296;お問い合せ&#12297;</span>山中温泉観光協会　TEL.0761-78-0330</p>
				</div>
			</div>
		</div>
	</div>
</section>

</div><!-- //#content -->
<?php endwhile; endif; ?>

<?php else: ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
<?php endif; get_footer(); ?>
