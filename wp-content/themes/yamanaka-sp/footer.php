<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<div id="topcontrol"><a href="#topIndex"><img src="<?php bloginfo('template_url'); ?>/common/images/gotop.png" alt="page top"></a></div>
<footer id="footer">
	<div id="finner">
		<ul>
			<li><a href="https://www.facebook.com/%E5%B1%B1%E4%B8%AD%E6%B8%A9%E6%B3%89%E8%A6%B3%E5%85%89%E5%8D%94%E4%BC%9A-251607508211337/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/btn_facebook.png" alt="facebook"></a></li>
		</ul>
		<p class="copyright"><small>Copyright©Yamanakaonsen All Right Reserved.</small></p>
	</div>
</footer>
</div><!-- #wrapper -->
</div>
</div>
<?php wp_footer(); ?>
</body>
</html>