<?php ?><!DOCTYPE html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta name="keywords" content="石川,温泉,山中,観光,観光協会">
<meta name="description" content="山中温泉観光協会のサイトです。開湯1300年の歴史と豊かな自然、文人墨客が愛した情緒あふれる温泉地です。">
<?php wp_head(); ?>
<meta name="viewport" content="user-scalable=yes" />
<meta name="format-detection" content="telephone=no" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/common.css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/top.css">
<script src="<?php bloginfo('template_url'); ?>/common/js/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/common.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/js/colorbox/colorbox.css" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/js/sidr/stylesheets/jquery.sidr.dark.css" />
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/sidr/jquery.sidr.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/colorbox/jquery.colorbox-min.js"></script>
<!--[if lt IE 9]>
<script src="<?php bloginfo('template_url'); ?>/common/js/html5shiv.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/css3-mediaqueries.js"></script>
<![endif]-->
<!-- <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/scrolltopcontrol.js"></script> -->

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/js/jquery.bxslider/jquery.bxslider.css">
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/skrollr-master/skrollr.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/jquery.matchHeight-min.js"></script>
<script type="text/javascript">
$(function(){
	$('.ic_pick').matchHeight();
	
	$('a[href^=#]').click(function(){
		var speed = 500;
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$("html, body").animate({scrollTop:position}, speed, "swing");
		return false;
	});

	$('#slides').bxSlider({
    pager: false,
		controls: true
  });
	
	$('.loopslider').bxSlider({
    pager: false,
		controls: false,
    auto:true,
    minSlides: 2,
    maxSlides: 2,
    slideWidth: 500,
    slideMargin: 10
  });
	
	$('#pla_cal').bxSlider({
		slideWidth: 500,
    minSlides: 3,
		maxSlides: 3,
		moveSlides: 1,
		slideMargin: 25,
    pager: false,
		controls: true
  });
});


</script>
</head>
<body id="topIndex">
<div id="wrapper">

	<header id="header" class="cf">
		<div class="inner">
			<h1 id="logo"><a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/common/images/logo.png" alt="日本の美と、渓谷の温泉と。山中温泉"></a></h1>
			<!--タップするリンク -->
			<p class="btn_area simple-menu" href="#sidr"><img src="<?php bloginfo('template_url'); ?>/common/images/btn_menu.png" alt="メニュー"></span></p>
			<!-- メニュー部分 -->
			<div id="sidr">
					<ul>
					<li><a href="<?php bloginfo('url'); ?>/">ホーム</a></li>
					<li><a href="<?php bloginfo('url'); ?>/concept/">コンセプト</a></li>
					<li><a href="<?php bloginfo('url'); ?>/about/">山中温泉について</a></li>
					<li><a href="<?php bloginfo('url'); ?>/hotel/">旅館紹介</a></li>
					<li><a href="<?php bloginfo('url'); ?>/highlights/">見どころ</a></li>
					<li><a href="<?php bloginfo('url'); ?>/event/">イベント</a></li>
					<li><a href="<?php bloginfo('url'); ?>/access/">アクセス</a></li>
					<li><a href="<?php bloginfo('url'); ?>/form/">お問い合わせ</a></li>
					<li><a href="<?php bloginfo('url'); ?>/link/">リンク集</a></li>
					<li><a href="<?php bloginfo('url'); ?>/download/">パンフレット</a></li>
					<li><a class="simple-menu" href="#sidr">閉じる</a></li>
					</ul>
			</div>
		</div>
<script>
$(document).ready(function() {
  $('.simple-menu').sidr({side: 'right'});
});
</script>
</header><!-- //#header -->

<div class="slideBg">
	<div class="slideWrap">
		<div class="slideWrap_inner">
			<div class="flexslider2">
				<ul id="slides">
				<?php
				if (have_posts()) : 
				while (have_posts()) : the_post();
				$repeat_group = scf::get('slide_area');
				foreach ( $repeat_group as $field_name => $field_value ) :
					$txt_link = $field_value['txt_link'];
					$val =  $field_value["slide_img"];
					echo '<li>';
					if (!empty($txt_link)) { echo '<a href="'.$txt_link.'">';}
					$image = wp_get_attachment_image_src($val, 'full');
					echo '<img src="'.$image[0].'">';
					if (!empty($txt_link)) { echo '</a>';}
					echo '</li>';
					
				 endforeach;
				 endwhile; endif; ?>
				</ul>
			</div>
		</div>
	</div>
	<div class="calWrap">
		<div class="calWrap_inner">
			<div id="loopBannerArea">
				<ul class="loopslider">
				<?php
				if (have_posts()) : 
				while (have_posts()) : the_post();
				$repeat_group = scf::get('slide_area_mini');
				foreach ( $repeat_group as $field_name => $field_value ) :
					$txt_link = $field_value['txt_mini_link'];
					$txt_txt = $field_value['txt_mini_txt'];
					$val =  $field_value["slide_mini_img"];
					$check_mini_check = $field_value['check_mini_check'];
					$youtube_url = $field_value['check_mini_url'];
					echo '<li>';
					if (!empty($txt_link)) { echo '<a href="'.$txt_link.'">';}
					if ($check_mini_check == '使う') {echo '<a href="https://www.youtube.com/embed/'.$youtube_url.'?rel=0" class="colorbox_youtube">';}
					$image = wp_get_attachment_image_src($val, 'full');
					echo '<img src="'.$image[0].'"><span>'.$txt_txt.'</span>';
					if (!empty($txt_link) or $check_mini_check == '使う') { echo '</a>';}
					echo '</li>';
					
				 endforeach;
				 endwhile; endif;wp_reset_postdata(); ?>
				</ul>
			</div>
		</div>
		<span class="slideBefore"></span>
		<span class="slideAfter"></span>
	</div>
</div>

	<div id="contents">

		<section class="conceptTop">
			<div class="inner miryoku">
				<h2 class="alignCenter"><img src="<?php bloginfo('template_url'); ?>/images/top/ttl01.png" alt="山中温泉の魅力"></h2>
				<p class="appTxt fo14 txt lheight">開湯1300年、山中温泉は豊かな自然と伝統文化が残る渓谷の温泉地です。<br>松尾芭蕉も愛してやまなかった美しい湯のまちで｢日本の心｣につかりませんか。</p>
				<ul class="bnr cf">
					<li><img src="<?php bloginfo('template_url'); ?>/images/top/app_01.jpg" class="bgov" alt=""><a href="<?php bloginfo('url'); ?>/about/"><img src="<?php bloginfo('template_url'); ?>/images/top/app_01_txt.png" alt=""></a></li>
					<li><img src="<?php bloginfo('template_url'); ?>/images/top/app_02.jpg" class="bgov" alt=""><a href="<?php bloginfo('url'); ?>/hotel/"><img src="<?php bloginfo('template_url'); ?>/images/top/app_02_txt.png" alt=""></a></li>
					<li><img src="<?php bloginfo('template_url'); ?>/images/top/app_03.jpg" class="bgov" alt=""><a href="<?php bloginfo('url'); ?>/highlights/"><img src="<?php bloginfo('template_url'); ?>/images/top/app_03_txt.png" alt=""></a></li>
					<li><img src="<?php bloginfo('template_url'); ?>/images/top/app_04.jpg" class="bgov" alt=""><a href="<?php bloginfo('url'); ?>/event/"><img src="<?php bloginfo('template_url'); ?>/images/top/app_04_txt.png" alt=""></a></li>
				</ul>
			</div>
		</section>


		<section class="pickUp">
			<div class="inner pickup">
				<h2 class="alignCenter"><img src="<?php bloginfo('template_url'); ?>/images/top/ttl02.png" alt="ピックアップ"></h2>
				<p class="pickTxt fo14 txt lheight">山中温泉の数ある魅力の中から、おすすめの情報をピックアップ。<br>訪れるたびに新しい出会いがあるのも、山中温泉の楽しみです。</p>
				<ul class="picklist cf">
				<?php
				query_posts(
					array(
					'post_type' => 'page',
					'page_id' => 227
					 ) 
				);
				// ループ
				if ( have_posts() ) : 
					while ( have_posts() ) : the_post();
				$pickuplist = array(1, 2, 3);
				foreach ($pickuplist as $pickup){
						$image_id = SCF::get('pickup_img_0'.$pickup);$image = wp_get_attachment_image_src($image_id, 'full');
						$pickup_title = SCF::get( 'pickup_title_0'.$pickup );
						$pickup_txt = SCF::get( 'pickup_txt_0'.$pickup );
						$pickup_url = SCF::get( 'pickup_url_0'.$pickup );
						if ($image[0]!="" or $pickup_title!="" ) { 
						echo '<li class="ic_pick"><a href="'.$pickup_url.'">';
						if ( $image[0] != ''){
								echo '<img src="'.$image[0].'" alt="">';
							}
						echo '<h4>'.esc_html( $pickup_title ).'</h4>
							<p>'.nl2br(esc_html( $pickup_txt )).'</p>
						</a></li>
						';
						}
				}
					endwhile; 
				endif;
				
				// クエリをリセット
				wp_reset_query();
			 ?>
				</ul>
			</div>
		</section><!-- /pickUp -->

		<section class="pla over_con">
			<h2 class="alignCenter"><img src="<?php bloginfo('template_url'); ?>/images/top/ttl03.png" alt="見どころ紹介"></h2>
			<p class="plaTxt fo14 txt lheight">歴史あるものも、新しいものも。豊かな文化も、美しい自然も。<br>ここにしかない多彩な見どころを、ぜひ体感してください。</p>
			<div class="pla_calWrap cf">
				<div class="pla_calInner">
					<div class="pla_calmr">
						<ul id="pla_cal">
						<?php the_content(); ?>
						</ul>
					</div>
				</div>
			</div>
			<div class="linkBtn"><a href="<?php bloginfo('url'); ?>/highlights/landmark/">一覧はこちら</a></div>
		</section><!-- /pla -->



		<section class="newsTop">
			<div class="inner">
				<h2 class="alignCenter pb25"><img src="<?php bloginfo('template_url'); ?>/images/top/ttl04.png" alt="ニュース NEWS"></h2>
				<div class="news">
					<ul class="fo14">

					<?php
					$news_cnt = 0;
					query_posts(
						array(
						'post_type' => 'news',
						'posts_per_page' => 3
						 ) 
					);
					if (have_posts()) : while (have_posts()) : the_post(); 
					$news_cnt++;
					?>
						<li class="cf <?php if(is_last_post()){ echo 'noln';}; ?>">
							<p class="ico"><img src="<?php bloginfo('template_url'); ?>/images/top/ico_news.gif" alt="NEWS"></p>
							<p class="txt"><span class="dates"><?php the_time('Y.m.d'); ?></span>
							<a href="<?php echo get_the_permalink(); ?>"><?php if(mb_strlen($post->post_title)>35) { $title= mb_substr($post->post_title,0,35) ; echo $title. ･･･ ;} else {echo $post->post_title;}?></a></p>
						</li>
					<?php endwhile; endif; ?>
					</ul>
				</div>
			<p class="linkBtn"><a href="<?php bloginfo('url'); ?>/news/">一覧はこちら</a></p>
			</div>
		</section>
</div><!-- //#content -->

<div id="topcontrol"><a href="#topIndex"><img src="<?php bloginfo('template_url'); ?>/common/images/gotop.png" alt="page top"></a></div>
<footer id="footer">
	<div id="finner">
		<ul>
			<li><a href="https://www.facebook.com/%E5%B1%B1%E4%B8%AD%E6%B8%A9%E6%B3%89%E8%A6%B3%E5%85%89%E5%8D%94%E4%BC%9A-251607508211337/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/btn_facebook.png" alt="facebook"></a></li>
		</ul>
		<p class="copyright"><small>Copyright©Yamanakaonsen All Right Reserved.</small></p>
	</div>
</footer>
</div><!-- #wrapper -->
</div>
</div>
<?php wp_footer(); ?>
</body>
</html>
