$(function(){
	var markers = [];
	var marker = []
	var infoWindow =[];
	var map = {};
	function initialize1() {
		var mapOptions = {
			//Map中心の座標 (石川県加賀市山中温泉白山町リ−４０)
			center: new google.maps.LatLng(36.251835, 136.372173),
			zoom: 14 //Mapのズーム
		};
		//Mapを表示する要素のIDを指定してMapを読み込み
		map = new google.maps.Map(document.getElementById('googlemap1'), mapOptions);
		// マーカーの情報を設定
		markers = [
			{
				position: new google.maps.LatLng(36.228354, 136.359275),
				title: '栢野大杉',
				summary: '栢野大杉の内容',
				figure: './common/images/common/img/p_05.jpg'
			},
			{
				position: new google.maps.LatLng(36.2377, 136.367516),
				title: '道の駅 ゆけむり健康村',
				summary: 'ゆけむり健康村の内容',
				figure: './common/images/common/img/p_06.jpg'
			},
			{
				position: new google.maps.LatLng(36.244627, 136.372146),
				title: 'アートギャラリー 耀',
				summary: 'アートギャラリー 耀の内容',
				figure: './common/images/common/img/p_07.jpg'
			},
			{
				position: new google.maps.LatLng(36.244827, 136.371276),
				title: 'いずも堂',
				summary: 'いずも堂 の内容',
				figure: './common/images/common/img/p_08.jpg'
			}
		]
		/* マーカーのアイコンの設定 */
		var image = {
			url: "./common/images/common/icon/arrow_red.png"//, //画像のURL
			// size: new google.maps.Size(32, 32), //サイズ
			// origin: new google.maps.Point(0, 0), //アイコンの基準位置
			// anchor: new google.maps.Point(16, 32), //アイコンのアンカーポイント
			// scaledSize: new google.maps.Size(32, 32) //アイコンのサイズ
		};
		/* マーカの実装 */
		for(var i = 0; i < markers.length; i++){
			marker[i] = new google.maps.Marker({
				position: markers[i].position,
				map: map,
				icon: image,
				title: markers[i].title,
				zIndex: markers.length - i
			});

			/* 場所の詳細の準備 */
			infoWindow[i] = new google.maps.InfoWindow({
				content: '<section style="margin-top:5px;"><figure style="float: left;"><img src="' + markers[i].figure + '" width="64px"></figure><div style="margin-left: 74px;"><h2 style="margin-bottom: 5px;font-size: 1.17em;">' + markers[i].title + '</h2><p style="font-size: 0.84em;">' + markers[i].summary + '</p></div></section>'
			});

			// マーカーをクリックしたら場所の詳細を表示
			google.maps.event.addListener(marker[i], 'click', function(e) {
				for(var i = 0; i < markers.length; i++) {
					if(marker[i].position.G == e.latLng.G && marker[i].position.K == e.latLng.K) {
						//クリックしたマーカーだったら詳細を表示
						infoWindow[i].open(map, marker[i]);
					} else {
						//クリックしたマーカーでなければ詳細を閉じる
						infoWindow[i].close();
					}
				}
			});
		}
	} // end - initialize
	google.maps.event.addDomListener(window, 'load', initialize1); //Google Map APIの実行
});