$(function(){
	var markers = [];
	var marker = [];
	var iw =[];
	var map = {};
	var lat_longs = [];

	//マーカーを管理するためのmarker_list
	var marker_list = new google.maps.MVCArray();

	function initialize() {
		var mapOptions = {
			//Map中心の座標 (石川県加賀市山中温泉白山町リ−４０)
			center: new google.maps.LatLng(36.251835, 136.372173),
			scrollwheel: false, //スクロール
			zoom: 14 //Mapのズーム
		};
		//Mapを表示する要素のIDを指定してMapを読み込み
		map = new google.maps.Map(document.getElementById('googlemap'), mapOptions);
		// マーカーの情報を設定

		jQuery.ajax({
			url: "../common/data/map2.json",
			async: true,
			type: 'GET',
			dataType:'json',
			cache: false,
			success: function(res) {

				// json読込用ループ
				for (var i = 0; i <= res.length-1; i++) {
					// 位置情報を指定
					var element = [{
						position: new google.maps.LatLng(res[i].lat, res[i].lng),
						title: res[i].name
					}]
					// プレイス情報をループ毎、markersに追加
					markers.push(element);

					// 下部 地点の名称一覧を生成する
					$("#spot_list").append("<li class="+res[i].placeGenre+"><a href=\"javascript:void(0);\" data-title=\""+res[i].name+"\">"+res[i].nameimg+"</a></li>");
				};

				// マーカーの実装開始
				for(var i = 0; i < markers.length; i++){
					// マーカーの情報を設定アイコンを指定
					if ( res[i].placeGenre == "hotel") { // 旅館(緑)
						var image = {
							url : "../common/images/common/icon/arrow_green.png"
						}
						var show = true
					} else
					if ( res[i].placeGenre == "famous") { // 名所(赤)
						var image = {
							url : "../common/images/common/icon/arrow_red.png"
						}
						var show = true
					} else
					if ( res[i].placeGenre == "gallery") { // お店・ギャラリー(お店・ギャラリー)
						var image = {
							url : "../common/images/common/icon/arrow_blue.png"
						}
						var show = true
					} else {
						map = null
					}

					// Mapの位置・ピンの画像などの表示設定
					marker[i] = new google.maps.Marker({
						position: markers[i][0].position,
						map: map,
						icon: image,
						visible: show,
						title: markers[i][0].title,
						zIndex: markers.length - i
					});

					// 最初に表示するMAPの位置を平均化する
					lat_longs.push(marker[i].getPosition());

					// マーカーの配置・再配置の管理用配列にPush
					marker_list.push(marker);

					// 場所の詳細の準備
					iw[i] = new google.maps.InfoWindow({
						content: "<div class=\"marker_wrap\"><h3 class=\"map_ttl\">"+marker[i].title+"</h3><p class=\"map_detail\">"+res[i].detail+"</p></div>"
					});

					// マーカーをクリックしたら場所の詳細を表示
					google.maps.event.addListener(marker[i], 'click', function(e) {
						for(var i = 0; i < markers.length; i++) {
							if(marker[i].position.G == e.latLng.G && marker[i].position.K == e.latLng.K) {
								//クリックしたマーカーだったら詳細を表示
								iw[i].open(map, marker[i]);
							} else {
								//クリックしたマーカーでなければ詳細を閉じる
								iw[i].close();
							}
						}
					});
				}

				// マーカーの実装完了

				// 対象のタイトルを持ったマーカーの詳細を開く
				$("#spot_list a").on('click', function() {
					for(var i = 0; i < markers.length; i++) {
						if(marker[i].title == $(this).attr("data-title")) {
							//マーカーとタイトルが一致したら詳細を表示
							iw[i].open(map, marker[i]);
						} else {
							//マーカーとタイトルが一致しなければ詳細を閉じる
							iw[i].close();
						}
					}
					return false;
				});

				// 最初に表示するMAPの位置を平均化する
				fitMapToBounds();

				/*--------------------
					Mapの切替 … 全部 ( 緑 / 赤 / 青 )
				--------------------*/
				var btn_all = document.getElementById("all");
				google.maps.event.addDomListener(btn_all, 'click', function() {
					// 表示されているマーカーの削除
					marker_list.forEach(function(marker,index) {
						marker[index].setMap(null);
					});

					// markersの中身を空にする
					markers = [];
					// マーカーの配置・再配置の管理用配列を空にする
					marker_list = new google.maps.MVCArray();

					// ページ下部 地点の名称一覧をリセットする
					$("#spot_list").empty();

					// json読込用ループ
					for (var i = 0; i <= res.length-1; i++) {

						// 位置情報を指定
						var element = [{
							position: new google.maps.LatLng(res[i].lat, res[i].lng),
							title: res[i].name
						}]
						// プレイス情報をループ毎、markersに追加
						markers.push(element);

						// 下部 地点の名称一覧を生成する
						$("#spot_list").append("<li class="+res[i].placeGenre+"><a href=\"javascript:void(0);\" data-title=\""+res[i].name+"\">"+res[i].nameimg+"</a></li>");

					};

					// マーカーの実装開始 - ここから
					for(var i = 0; i < markers.length; i++){

						// マーカーの情報を設定アイコンを指定
						if ( res[i].placeGenre == "hotel") { // 旅館(緑)
							var image = {
								url : "../common/images/common/icon/arrow_green.png"
							}
							var show = true
						} else
						if ( res[i].placeGenre == "famous") { // 名所(赤)
							var image = {
								url : "../common/images/common/icon/arrow_red.png"
							}
							var show = true
						} else
						if ( res[i].placeGenre == "gallery") { // お店・ギャラリー(お店・ギャラリー)
							var image = {
								url : "../common/images/common/icon/arrow_blue.png"
							}
							var show = true
						} else {
							map = null
						}

						// Mapの位置・ピンの画像などの表示設定
						marker[i] = new google.maps.Marker({
							position: markers[i][0].position,
							map: map,
							icon: image,
							visible: show,
							title: markers[i][0].title,
							zIndex: markers.length - i
						});

						// マーカーの配置・再配置の管理用配列にPush
						marker_list.push(marker);

						// 場所の詳細の準備
						iw[i] = new google.maps.InfoWindow({
							content: "<div class=\"marker_wrap\"><h3 class=\"map_ttl\">"+marker[i].title+"</h3><p class=\"map_detail\">"+res[i].detail+"</p></div>"
						});

						// マーカーをクリックしたら場所の詳細を表示
						google.maps.event.addListener(marker[i], 'click', function(e) {
							for(var i = 0; i < markers.length; i++) {
								if(marker[i].position.G == e.latLng.G && marker[i].position.K == e.latLng.K) {
									//クリックしたマーカーだったら詳細を表示
									iw[i].open(map, marker[i]);
								} else {
									//クリックしたマーカーでなければ詳細を閉じる
									iw[i].close();
								}
							}
						});

					} // マーカーの実装開始 - ここまで

					// 順番が変更されるので再度読みなおす ( 対象のタイトルを持ったマーカーの詳細を開く )
					$("#spot_list a").on('click', function() {
						for(var i = 0; i < markers.length; i++) {
							if(marker[i].title == $(this).attr("data-title")) {
								//マーカーとタイトルが一致したら詳細を表示
								iw[i].open(map, marker[i]);
							} else {
								//マーカーとタイトルが一致しなければ詳細を閉じる
								iw[i].close();
							}
						}
						return false;
					});

				});

				/*--------------------
					Mapの切替 … 旅館/緑
				--------------------*/
				var btn_green = document.getElementById("green");
				google.maps.event.addDomListener(btn_green, 'click', function() {
					// 表示されているマーカーの削除
					marker_list.forEach(function(marker,index) {
						marker[index].setMap(null);
					});

					// markersの中身を空にする
					markers = [];
					// マーカーの配置・再配置の管理用配列を空にする
					marker_list = new google.maps.MVCArray();

					// ページ下部 地点の名称一覧をリセットする
					$("#spot_list").empty();

					// json読込用ループ
					for (var i = 0; i <= res.length-1; i++) {

						// 位置情報を指定
						var element = [{
							position: new google.maps.LatLng(res[i].lat, res[i].lng),
							title: res[i].name
						}]
						// プレイス情報をループ毎、markersに追加
						markers.push(element);

						// 下部 地点の名称一覧を生成する
						$("#spot_list").append("<li class="+res[i].placeGenre+"><a href=\"javascript:void(0);\" data-title=\""+res[i].name+"\">"+res[i].nameimg+"</a></li>");

					};

					// ページ下部 地点の名称一覧 不要なスポットを削除する
					$("#spot_list").find("li.famous,li.gallery").remove();

					// マーカーの実装開始 - ここから
					for(var i = 0; i < markers.length; i++){

						// マーカーの情報を設定アイコンを指定
						if ( res[i].placeGenre == "hotel") { // 旅館(緑)
							var image = {
								url : "../common/images/common/icon/arrow_green.png"
							}
							var show = true
						} else {
							var show = false
						}

						// Mapの位置・ピンの画像などの表示設定
						marker[i] = new google.maps.Marker({
							position: markers[i][0].position,
							map: map,
							icon: image,
							visible: show,
							title: markers[i][0].title,
							zIndex: markers.length - i
						});

						// マーカーの配置・再配置の管理用配列にPush
						marker_list.push(marker);

						// 場所の詳細の準備
						iw[i] = new google.maps.InfoWindow({
							content: "<div class=\"marker_wrap\"><h3 class=\"map_ttl\">"+marker[i].title+"</h3><p class=\"map_detail\">"+res[i].detail+"</p></div>"
						});

						// マーカーをクリックしたら場所の詳細を表示
						google.maps.event.addListener(marker[i], 'click', function(e) {
							for(var i = 0; i < markers.length; i++) {
								if(marker[i].position.G == e.latLng.G && marker[i].position.K == e.latLng.K) {
									//クリックしたマーカーだったら詳細を表示
									iw[i].open(map, marker[i]);
								} else {
									//クリックしたマーカーでなければ詳細を閉じる
									iw[i].close();
								}
							}
						});

					} // マーカーの実装開始 - ここまで

					// 順番が変更されるので再度読みなおす ( 対象のタイトルを持ったマーカーの詳細を開く )
					$("#spot_list a").on('click', function() {
						for(var i = 0; i < markers.length; i++) {
							if(marker[i].title == $(this).attr("data-title")) {
								//マーカーとタイトルが一致したら詳細を表示
								iw[i].open(map, marker[i]);
							} else {
								//マーカーとタイトルが一致しなければ詳細を閉じる
								iw[i].close();
							}
						}
						return false;
					});

				});

				/*--------------------
					Mapの切替 … 旅館/赤
				--------------------*/
				var btn_red = document.getElementById("red");
				google.maps.event.addDomListener(btn_red, 'click', function() {
					// 表示されているマーカーの削除
					marker_list.forEach(function(marker,index) {
						marker[index].setMap(null);
					});

					// markersの中身を空にする
					markers = [];
					// マーカーの配置・再配置の管理用配列を空にする
					marker_list = new google.maps.MVCArray();

					// ページ下部 地点の名称一覧をリセットする
					$("#spot_list").empty();

					// json読込用ループ
					for (var i = 0; i <= res.length-1; i++) {

						// 位置情報を指定
						var element = [{
							position: new google.maps.LatLng(res[i].lat, res[i].lng),
							title: res[i].name
						}]
						// プレイス情報をループ毎、markersに追加
						markers.push(element);

						// 下部 地点の名称一覧を生成する
						$("#spot_list").append("<li class="+res[i].placeGenre+"><a href=\"javascript:void(0);\" data-title=\""+res[i].name+"\">"+res[i].nameimg+"</a></li>");

					};

					// ページ下部 地点の名称一覧 不要なスポットを削除する
					$("#spot_list").find("li.hotel,li.gallery").remove();

					// マーカーの実装開始 - ここから
					for(var i = 0; i < markers.length; i++){

						// マーカーの情報を設定アイコンを指定
						if ( res[i].placeGenre == "famous") { // 観光地(赤)
							var image = {
								url : "../common/images/common/icon/arrow_red.png"
							}
							var show = true
						} else {
							var show = false
						}

						// Mapの位置・ピンの画像などの表示設定
						marker[i] = new google.maps.Marker({
							position: markers[i][0].position,
							map: map,
							icon: image,
							visible: show,
							title: markers[i][0].title,
							zIndex: markers.length - i
						});

						// マーカーの配置・再配置の管理用配列にPush
						marker_list.push(marker);

						// 場所の詳細の準備
						iw[i] = new google.maps.InfoWindow({
							content: "<div class=\"marker_wrap\"><h3 class=\"map_ttl\">"+marker[i].title+"</h3><p class=\"map_detail\">"+res[i].detail+"</p></div>"
						});

						// マーカーをクリックしたら場所の詳細を表示
						google.maps.event.addListener(marker[i], 'click', function(e) {
							for(var i = 0; i < markers.length; i++) {
								if(marker[i].position.G == e.latLng.G && marker[i].position.K == e.latLng.K) {
									//クリックしたマーカーだったら詳細を表示
									iw[i].open(map, marker[i]);
								} else {
									//クリックしたマーカーでなければ詳細を閉じる
									iw[i].close();
								}
							}
						});

					} // マーカーの実装開始 - ここまで

					// 順番が変更されるので再度読みなおす ( 対象のタイトルを持ったマーカーの詳細を開く )
					$("#spot_list a").on('click', function() {
						for(var i = 0; i < markers.length; i++) {
							if(marker[i].title == $(this).attr("data-title")) {
								//マーカーとタイトルが一致したら詳細を表示
								iw[i].open(map, marker[i]);
							} else {
								//マーカーとタイトルが一致しなければ詳細を閉じる
								iw[i].close();
							}
						}
						return false;
					});

				});

				/*--------------------
					Mapの切替 … お店・ギャラリー/青
				--------------------*/
				var btn_blue = document.getElementById("blue");
				google.maps.event.addDomListener(btn_blue, 'click', function() {
					// 表示されているマーカーの削除
					marker_list.forEach(function(marker,index) {
						marker[index].setMap(null);
					});

					// markersの中身を空にする
					markers = [];
					// マーカーの配置・再配置の管理用配列を空にする
					marker_list = new google.maps.MVCArray();

					// ページ下部 地点の名称一覧をリセットする
					$("#spot_list").empty();

					// json読込用ループ
					for (var i = 0; i <= res.length-1; i++) {

						// 位置情報を指定
						var element = [{
							position: new google.maps.LatLng(res[i].lat, res[i].lng),
							title: res[i].name
						}]
						// プレイス情報をループ毎、markersに追加
						markers.push(element);

						// 下部 地点の名称一覧を生成する
						$("#spot_list").append("<li class="+res[i].placeGenre+"><a href=\"javascript:void(0);\" data-title=\""+res[i].name+"\">"+res[i].nameimg+"</a></li>");

					};

					// ページ下部 地点の名称一覧 不要なスポットを削除する
					$("#spot_list").find("li.hotel,li.famous").remove();

					// マーカーの実装開始 - ここから
					for(var i = 0; i < markers.length; i++){

						// マーカーの情報を設定アイコンを指定
						if ( res[i].placeGenre == "gallery") { // 観光地(赤)
							var image = {
								url : "../common/images/common/icon/arrow_blue.png"
							}
							var show = true
						} else {
							var show = false
						}

						// Mapの位置・ピンの画像などの表示設定
						marker[i] = new google.maps.Marker({
							position: markers[i][0].position,
							map: map,
							icon: image,
							visible: show,
							title: markers[i][0].title,
							zIndex: markers.length - i
						});

						// マーカーの配置・再配置の管理用配列にPush
						marker_list.push(marker);

						// 場所の詳細の準備
						iw[i] = new google.maps.InfoWindow({
							content: "<div class=\"marker_wrap\"><h3 class=\"map_ttl\">"+marker[i].title+"</h3><p class=\"map_detail\">"+res[i].detail+"</p></div>"
						});

						// マーカーをクリックしたら場所の詳細を表示
						google.maps.event.addListener(marker[i], 'click', function(e) {
							for(var i = 0; i < markers.length; i++) {
								if(marker[i].position.G == e.latLng.G && marker[i].position.K == e.latLng.K) {
									//クリックしたマーカーだったら詳細を表示
									iw[i].open(map, marker[i]);
								} else {
									//クリックしたマーカーでなければ詳細を閉じる
									iw[i].close();
								}
							}
						});

					} // マーカーの実装開始 - ここまで

					// 順番が変更されるので再度読みなおす ( 対象のタイトルを持ったマーカーの詳細を開く )
					$("#spot_list a").on('click', function() {
						for(var i = 0; i < markers.length; i++) {
							if(marker[i].title == $(this).attr("data-title")) {
								//マーカーとタイトルが一致したら詳細を表示
								iw[i].open(map, marker[i]);
							} else {
								//マーカーとタイトルが一致しなければ詳細を閉じる
								iw[i].close();
							}
						}
						return false;
					});

				});


				// 最初に表示するMAPの位置を平均化する
				function fitMapToBounds() {
					var bounds = new google.maps.LatLngBounds();
					if (lat_longs.length>0) {
						for (var i=0; i<lat_longs.length; i++) {
							bounds.extend(lat_longs[i]);
						}
						map.fitBounds(bounds);
					}
				} // end - fitMapToBounds

				function createMarker(markerOptions) {
					var marker = new google.maps.Marker(markerOptions);
					markers.push(marker);
					marker_list.push(marker);
					lat_longs.push(marker.getPosition());
					return marker;
				} // end - createMarker

			}
		});
	} // end - initialize

	google.maps.event.addDomListener(window, 'load', initialize); //Google Map APIの実行
});