var my_google_map;
var my_google_geo;

function googlemap_init( id_name, addr_name ) {
	var latlng = new google.maps.LatLng(41, 133);
	var opts = {
		zoom: 16,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		center: latlng,
		scrollwheel: false
	};
	my_google_map = new google.maps.Map(document.getElementById(id_name), opts);

	my_google_geo = new google.maps.Geocoder();
	var req = {
		address: addr_name ,
	};
}