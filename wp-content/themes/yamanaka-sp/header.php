<?php global $post; ?>
<!DOCTYPE html>
<head prefix="og: https://ogp.me/ns# fb: https://ogp.me/ns/fb# website: https://ogp.me/ns/website#">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta name="keywords" content="石川,温泉,山中,観光,観光協会">
<meta name="description" content="山中温泉観光協会のサイトです。開湯1300年の歴史と豊かな自然、文人墨客が愛した情緒あふれる温泉地です。">
<?php wp_head(); ?>
<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> -->
<meta name="viewport" content="user-scalable=yes" />
<meta name="format-detection" content="telephone=no" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/common.css">
<script src="<?php bloginfo('template_url'); ?>/common/js/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/common.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/js/colorbox/colorbox.css" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/js/sidr/stylesheets/jquery.sidr.dark.css" />
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/sidr/jquery.sidr.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/colorbox/jquery.colorbox-min.js"></script>
<?php if (is_post_type_archive('eventdetail') or is_post_type_archive('event') or is_singular('eventdetail') or is_singular('event')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/event.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/editor-style.css" />
<?php
endif; 
if (is_post_type_archive('news') or is_tax('news_cat') or is_singular('news')) : ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/news.css">
	<script>
	$(function(){$(".colorbox").colorbox();});
	</script>
<?php
endif; 
if (is_page('about')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/about.css">
<?php
endif; 
if (is_page('access')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/access.css">
	<script src="https://maps.google.com/maps/api/js"></script>
<script>
/* DOMの読み込み完了後に処理 */
google.maps.event.addDomListener( window , 'load', googleMapsSyncer );

//地図のインスタンスを格納する変数
var map = null ;

//マーカーのインスタンスを格納する変数
var markers = [];

//Google Mapsのプログラムを実行する
function googleMapsSyncer(){

//キャンパスの要素を取得する
var canvas = document.getElementById( "map-canvas" );

//中心の位置座標を指定する
var latlng = new google.maps.LatLng( 36.251946 , 136.374303 );

//地図のオプションを設定する
var mapOptions = {
zoom: 15 ,				//ズーム値
center: latlng ,		//中心座標 [latlng]
scrollwheel: false
};

//[canvas]に、[mapOptions]の内容の、地図のインスタンス([map])を作成する
map = new google.maps.Map( canvas , mapOptions );

//マーカーを作成、設置する
//markers[1]、markers[2]と増やしていくと、後々の管理が楽になります。
markers[0] = new google.maps.Marker( { map: map , position: latlng , icon:'../common/images/common/icon/map_marker.png' } );

}
</script>
<script>
$(function(){
	$('a.kakudai').colorbox();
});
</script>
<?php
endif; 
if (is_page('concept')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/concept.css">
<?php
endif; 
if (is_page('download')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/download.css">
<?php
endif; 
if (is_page('form')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/form.css">
	<script src="https://maps.google.com/maps/api/js"></script>
	<script>
	/* DOMの読み込み完了後に処理 */
	google.maps.event.addDomListener( window , 'load', googleMapsSyncer );
	
	//地図のインスタンスを格納する変数
	var map = null ;
	
	//マーカーのインスタンスを格納する変数
	var markers = [];
	
	//Google Mapsのプログラムを実行する
	function googleMapsSyncer(){
	
	//キャンパスの要素を取得する
	var canvas = document.getElementById( "map-canvas" );
	
	//中心の位置座標を指定する
	var latlng = new google.maps.LatLng( 36.251946 , 136.374303 );
	
	//地図のオプションを設定する
	var mapOptions = {
	zoom: 15 ,				//ズーム値
	center: latlng ,		//中心座標 [latlng]
	scrollwheel: false
	};
	
	//[canvas]に、[mapOptions]の内容の、地図のインスタンス([map])を作成する
	map = new google.maps.Map( canvas , mapOptions );
	
	//マーカーを作成、設置する
	//markers[1]、markers[2]と増やしていくと、後々の管理が楽になります。
	markers[0] = new google.maps.Marker( { map: map , position: latlng , icon:'<?php bloginfo('template_url'); ?>/common/images/common/icon/map_marker.png' } );
	
	}
	</script>
<?php
endif; 
if (is_page('highlights') || is_parent_slug() === 'highlights' || is_parent_slug() === 'walk' || is_parent_slug() === 'map'): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/highlights.css">
<script language="JavaScript">
$(function(){
	$(".inline").colorbox({
		inline:true,
		width:980,
		height:550
	});
});
</script>
<script>
$(function(){
	$('area').on('click', function(){
		var elm = $(this).attr('data-class'); //クリックされたareaタグの属性を取得
		var offsetTop = $('#'+elm).offset().top; //クリックされたボタンと同じIDを持つ要素のTOP位置を取得
		var trg = offsetTop - 90; //要素のTOP位置から−90pxする(ヘッダーなどの事を考慮)
		$('body, html').animate({scrollTop: trg}, 500); //0.5秒かけて、それぞれに移動
	});
});
</script>
<?php
endif; 
if (is_page('walk')): ?>
<script type="text/javascript">
	$(window).on('load resize', function(){
				var thumb_height = $('.picList li>img').height();
				$('.picList li a').css("height",thumb_height);
});
</script>
<?php
endif; 
if (is_page('map')): ?>
	<script src="https://maps.google.com/maps/api/js"></script>
	<script src="<?php bloginfo('template_url'); ?>/common/js/map.js"></script>
<?php
endif; 
if (is_page('highlights')): ?>
<script>
$(function(){
     $(".picList li").click(function(){
         window.location=$(this).find("a").attr("href");
         return false;
    });
});
</script>
<?php
endif; 
if (is_page('landmark')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/highlights.css">
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/jquery.tile.min.js"></script>
<?php
endif; 
if (is_page('hotel')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/hotel.css">
<script>
$(function () {
    var columns = 2;
    var liLen = $('.list_area li').length;
    var lineLen = Math.ceil(liLen / columns);
    var liHiArr = [];
    var lineHiArr = [];

    for (i = 0; i <= liLen; i++) {
        liHiArr[i] = $('.list_area li').eq(i).height();
        if (lineHiArr.length <= Math.floor(i / columns) 
            || lineHiArr[Math.floor(i / columns)] < liHiArr[i]) 
        {
            lineHiArr[Math.floor(i / columns)] = liHiArr[i];
        }
    }
    $('.list_area li').each(function(i){
        $(this).css('height',lineHiArr[Math.floor(i / columns)] + 'px');
    });
		
});
</script>
<?php
endif; 
if (is_page('link')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/link.css">
<?php
endif; 
if (is_page('route')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/route.css">
<?php endif; ?>
<!--[if lt IE 9]>
<script src="<?php bloginfo('template_url'); ?>/common/js/html5shiv.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/css3-mediaqueries.js"></script>
<![endif]-->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/skrollr-master/skrollr.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/jquery.matchHeight-min.js"></script>
<script type="text/javascript">
$(function(){
	
	$('a[href^=#]').click(function(){
		var speed = 500;
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$("html, body").animate({scrollTop:position}, speed, "swing");
		return false;
	});
});


</script>
<!-- googleアクセス解析 --> 
</script>
<script type="text/javascript">
_uacct = "UA-928173-9";
urchinTracker();
swfobject.registerObject("FlashID");
</script>
<!-- /googleアクセス解析 --> 

</head>

<body <?php body_class(); ?>>
<body id="topIndex">
<div id="wrapper">

	<header id="header" class="cf">
		<div class="inner">
			<h1 id="logo"><a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/common/images/logo.png" alt="日本の美と、渓谷の温泉と。山中温泉"></a></h1>
			<!--タップするリンク -->
			<p class="btn_area simple-menu" href="#sidr"><img src="<?php bloginfo('template_url'); ?>/common/images/btn_menu.png" alt="メニュー"></span></p>
			<!-- メニュー部分 -->
			<div id="sidr">
					<ul>
					<li><a href="<?php bloginfo('url'); ?>/">ホーム</a></li>
					<li><a href="<?php bloginfo('url'); ?>/concept/">コンセプト</a></li>
					<li><a href="<?php bloginfo('url'); ?>/about/">山中温泉について</a></li>
					<li><a href="<?php bloginfo('url'); ?>/hotel/">旅館紹介</a></li>
					<li><a href="<?php bloginfo('url'); ?>/highlights/">見どころ</a></li>
					<li><a href="<?php bloginfo('url'); ?>/event/">イベント</a></li>
					<li><a href="<?php bloginfo('url'); ?>/access/">アクセス</a></li>
					<li><a href="<?php bloginfo('url'); ?>/form/">お問い合わせ</a></li>
					<li><a href="<?php bloginfo('url'); ?>/link/">リンク集</a></li>
					<li><a href="<?php bloginfo('url'); ?>/download/">パンフレット</a></li>
					<li><a class="simple-menu" href="#sidr">閉じる</a></li>
					</ul>
			</div>
		</div>
<script>
$(document).ready(function() {
  $('.simple-menu').sidr({side: 'right'});
});
</script>
</header><!-- //#header -->

