<?php get_header(); ?>
	<section class="mainimg">
		<h1 class="headTitle"><img src="<?php bloginfo('template_url'); ?>/images/news/ttl.png" width="102" height="50" alt="ニュース NEWS"></h1>
	</section>

<div id="contents">
	<ul class="path">
		<li><a href="<?php bloginfo('url'); ?>">ホーム</a>&#65310;</li>
		<li><a href="<?php bloginfo('url'); ?>/news/">ニュース</a>&#65310;</li>
		<li><?php the_title(); ?></li>
	</ul>

	<section>
		<div class="inner clearfix">
	
			<h2 class="cate_acmenu"><img src="<?php bloginfo('template_url'); ?>/images/news/arrow_cate.jpg" width="100%" alt="">カテゴリ</h2>
			<ul class="loglist">
				<?php wp_list_categories(array('title_li' => '', 'taxonomy' => 'news_cat')); ?>
			</ul>
			<?php if (have_posts()) : 
			while (have_posts()) : the_post(); ?>
			<div class="art">
				<h3 class="artttl"><?php the_title(); ?></h3>
				<div class="time"><?php the_time('Y年m月d日'); ?></div>
				<div class="mceContentBody">
						<?php the_content(); ?>
				</div>
			</div>
	
			<div class="metadata">
				<div class="metainner cf">
					<dl class="categ cf">
						<dt>カテゴリー：</dt>
						<dd><?php $terms = get_the_terms( get_the_ID(), 'news_cat' );
								if ( !empty($terms) ) {
									if ( !is_wp_error( $terms ) ) {
										foreach( $terms as $term ) {
											echo $term->name;
												if ($term !== end($terms)) {
													echo '、';
												}
										}
									}
								} ?></dd>
					</dl>
				</div>
			</div>
	
			<ul class="newspager">
				<li class="toprev">
				<?php $next_post = get_next_post(); 
				$prev_poxt = get_previous_post(); 
				if (!empty( $next_post )): ?>
				<a href="<?php echo get_permalink( $next_post->ID );  ?>"><img src="<?php bloginfo('template_url'); ?>/images/news/btn_prev.png" width="100%" alt="前へ"></a></li>
				<?php endif; ?>&nbsp;</li>
				<li class="tolist"><a href="<?php bloginfo('url'); ?>/news/"><img src="<?php bloginfo('template_url'); ?>/images/news/btn_list.png" width="100%" alt="一覧を見る"></a></li>
				<li class="tonext">
				<?php if (!empty( $prev_poxt  )): ?>
				<a href="<?php echo get_permalink( $prev_poxt->ID ); ?>"><img src="<?php bloginfo('template_url'); ?>/images/news/btn_next.png" width="100%" alt="次へ"></a>
				<?php endif; ?>&nbsp;</li>
				
				
			</ul>
			<?php endwhile; endif; wp_reset_query();?>
	
			<h3 class="logttl"><span>過去のお知らせ</span></h3>
			<ul class="loglist">
				<?php wp_get_archives('type=monthly&post_type=news&format=html'); ?>
			</ul>
	
		</div>
	</section>




	</div><!-- //#content -->
<?php get_footer(); ?>
