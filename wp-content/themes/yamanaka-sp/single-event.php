<?php get_header(); ?>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/scrolltopcontrol.js"></script>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>


<section class="mainimg">
<h1 class="headTitle"><img src="<?php bloginfo('template_url'); ?>/images/event/ttl.png" alt="イベント情報"></h1>
</section>
<div id="contents">

<section>
	<div class="inner eventDetail">
	<?php
	$image_id = SCF::get('img_main');
	$image = wp_get_attachment_image_src($image_id, 'full');
	if (!empty($image_id)) {
		echo '<p class="thumb"><img src="'.$image[0].'"></p>';
	};
	?>
	<h2><?php $val = nl2br(get_post_meta($post->ID, 'txt_big', true));
				if (!empty($val)){
					echo '<span>'.$val.'</span><br>';
				} the_title(); ?></h2>
	<?php
				$val = nl2br(get_post_meta($post->ID, 'txt_main', true));
				if (!empty($val)){
					echo '<div class="main_txt">'.$val.'</div>';
				}
				?>
	
		<?php 
		$for_cnt = 0;
		$args = array(
						'connected_type' => 'posts_to_events',
						'connected_items' =>  $post->ID,
						'nopaging' => true,
						'suppress_filters' => false
		);
		$connected_posts = get_posts( $args ); 
		
		foreach ( $connected_posts as $post ) {
		setup_postdata( $post );
		$for_cnt++; 
		
		$txt_date =  scf::get('txt_date');
		$txt_time =  scf::get('txt_time');
		$txt_price =  scf::get('txt_price');
		$txt_area =  scf::get('txt_area');
		$txt_other =  scf::get('txt_other');
		$link_url =  scf::get('link_url');
		$link_file =  scf::get('link_file');
		$txt_otoiawase =  scf::get('txt_otoiawase');
		?>
	<h3><img src="<?php bloginfo('template_url'); ?>/images/event/ttl_event.png" alt=""></h3>
	<div class="detail_txt">
				<?php if ( $for_cnt == 1 ) : ?>
				<!--<h3><img src="<?php bloginfo('template_url'); ?>/images/event/page/event_deta.png" width="136" height="25" alt=""></h3>-->
				<?php
				endif;
				if (!empty($link_url)):
				?>
				<dl class="cf">
					<dd class="link_file mceContentBody"><a href="<?php
					echo get_post_meta($post->ID, 'link_url', true);
					?>" target="_blank">詳細はこちらをご覧ください。</a></dd>
				</dl>
				<?php endif;
				if (!empty($link_file)):
				?>
				<dl class="cf">
					<dd class="link_file mceContentBody"><a href="<?php
					$file_id = SCF::get('link_file');
					$file = wp_get_attachment_url($file_id);
					echo $file;
					?>" target="_blank">チラシのダウンロードはこちら。</a></dd>
				</dl>
				<?php
				endif;
				if (!empty($txt_date)):	?>
				<dl class="cf">
					<dt>&#12296;期 間&#12297;</dt>
					<dd><?php echo nl2br(get_post_meta($post->ID, 'txt_date', true)); ?></dd>
				</dl>
				<?php
				endif;
				if (!empty($txt_time)):
				?>
				<dl class="cf">
					<dt>&#12296;時 間&#12297;</dt>
					<dd><?php echo nl2br(get_post_meta($post->ID, 'txt_time', true)); ?></dd>
				</dl>
				<?php
				endif;
				if (!empty($txt_price)):
				?>
				<dl class="cf">
					<dt>&#12296;料 金&#12297;</dt>
					<dd><?php echo nl2br(get_post_meta($post->ID, 'txt_price', true)); ?></dd>
				</dl>
				<?php
				endif;
				if (!empty($txt_area)):
				?>
				<dl class="cf">
					<dt>&#12296;場 所&#12297;</dt>
					<dd><?php echo nl2br(get_post_meta($post->ID, 'txt_area', true)); ?></dd>
				</dl>
				<?php
				endif;
				if (!empty($txt_other)):
				?>
				<dl class="cf">
					<dd class="txt_other mceContentBody"><?php echo wpautop($txt_other); ?></dd>
				</dl>
				<?php
				endif;
				if (!empty($txt_otoiawase)):
				?>
				<p><span class="pr20">&#12296;お問い合せ&#12297;</span><?php echo nl2br(get_post_meta($post->ID, 'txt_otoiawase', true)); ?></p>
				<?php	endif;	?>
	</div>


	<ul class="thumb_list cf">
			<?php
				$repeat_group = scf::get('img_area');
				foreach ( $repeat_group as $field_name => $field_value ) :
				$val =  $field_value["img_list"];
					if (!empty($val)) {
					$image = wp_get_attachment_image_src($val, 'full');
					echo '<li><img src="'.$image[0].'"></p>';
					}
			 endforeach; ?>
	</ul>

		<?php } wp_reset_postdata();?>
		
	<p class="linkBtn"><a href="<?php bloginfo('url'); ?>/event/">イベント情報ページヘ</a></p>
	</div>
</section>

</div><!-- //#content -->
<?php endwhile; endif; ?>
<?php get_footer(); ?>
