<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header(); ?>
	<section class="mainimg">
		<h1 class="headTitle"><img src="<?php bloginfo('template_url'); ?>/images/news/ttl.png" width="102" height="50" alt="ニュース NEWS"></h1>
	</section>

	<div id="contents">
		<ul class="path">
			<li><a href="<?php bloginfo('url'); ?>">ホーム</a>&#65310;</li>
			<li>ニュース</li>
		</ul>
		

<section>
	<div class="inner clearfix">

		<h2 class="cate_acmenu"><img src="<?php bloginfo('template_url'); ?>/images/news/arrow_cate.jpg" width="100%" alt="">カテゴリ</h2>
		<ul class="loglist">
			<?php wp_list_categories(array('title_li' => '', 'taxonomy' => 'news_cat')); ?>
		</ul>

		<ul class="newsList">
				<?php if (have_posts()) : 
					$news_cnt = 0;
					while (have_posts()) : the_post();
					$news_cnt++; ?>
				<li><a href="<?php echo get_the_permalink(); ?>">
					<span><?php the_time('Y年m月d日'); ?></span>
					<h4><?php the_title(); ?></h4>
					<img src="<?php bloginfo('template_url'); ?>/images/news/img_more.jpg" width="100%" class="more" alt="">
				</a></li>
				<?php endwhile; endif; wp_reset_query();?>
		</ul>

		<ul class="newspager">
			<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
		</ul>

		<h3 class="logttl"><span>過去のお知らせ</span></h3>
		<ul class="loglist">
			<?php wp_get_archives('type=monthly&post_type=news&format=html'); ?>
		</ul>

	</div>
</section> 



</div><!-- //#content -->
<?php get_footer(); ?>
