<?php get_header(); ?>
<script>
$(window).on('load resize', function() {
    var columns = 2;
    var liLen = $('.event_list li').length;
    var lineLen = Math.ceil(liLen / columns);
    var liHiArr = [];
    var lineHiArr = [];

    for (i = 0; i <= liLen; i++) {
        liHiArr[i] = $('.event_list li').eq(i).height();
        if (lineHiArr.length <= Math.floor(i / columns) 
            || lineHiArr[Math.floor(i / columns)] < liHiArr[i]) 
        {
            lineHiArr[Math.floor(i / columns)] = liHiArr[i];
        }
    }
    $('.event_list li').each(function(i){
        $(this).css('height',lineHiArr[Math.floor(i / columns)] + 'px');
    });
});
</script>
<section class="mainimg">
<h1 class="headTitle"><img src="<?php bloginfo('template_url'); ?>/images/event/ttl.png" width="98" height="42" alt="イベント EVENT"></h1>
</section>

<div id="contents">
<ul class="path">
	<li><a href="<?php bloginfo('url'); ?>">ホーム</a>&#65310;</li>
	<li>イベント</li>
</ul>

<section>
	<div class="inner eventArea">
	<h2>山中温泉では、四季折々のまつりやイベントが開催されています。<br>人と自然、伝統と文化にふれるひとときをお楽しみください。</h2>
	<h3><img src="<?php bloginfo('template_url'); ?>/images/event/title01.png" alt="新着イベント情報"></h3>
	<p class="txt">現在開催中・近日開催予定のイベントをご案内いたします。</p>
	<ul class="new_event">
		<?php 
		query_posts(
			array(
			'post_type' => 'eventdetail',
			'posts_per_page' => 3
			 ) 
		);
		$for_cnt = 0;
		if(have_posts()): while(have_posts()): the_post(); ?>
		<li>
			<?php 
			$args = array(
							'connected_type' => 'posts_to_events',
							'connected_items' =>  $post->ID,
							'nopaging' => true,
							'suppress_filters' => false
			);
			$connected_posts = get_posts( $args );
			foreach ( $connected_posts as $post ) {
			// タイトル
			$title = get_the_title();
			echo '<h4>'.$title.'</h4>';
			setup_postdata( $post ); 
			// 画像
			$image_id = SCF::get('img_main');
			$image = wp_get_attachment_image_src($image_id, 'full');
			if (!empty($image_id)) {
				echo '<p class="thumb"><span><img src="'.get_bloginfo('template_url').'/images/event/new.png" alt=""></span><img src="'.$image[0].'" alt=""></p>';
			};
			
			
			// リンク
			$parm = get_permalink();

			} wp_reset_postdata();?>
			<div class="ex_txt">
				<?php
				$txt_date =  scf::get('txt_date');
				$txt_time =  scf::get('txt_time');
				$txt_area =  scf::get('txt_area');
				
				if (!empty($txt_date)) {
					echo '&#12296;日 時&#12297; ';
					$date_ver = strip_tags(get_post_meta($post->ID, 'txt_date', true));
					echo mb_substr($date_ver,0,40);
					if ( mb_strlen($date_ver) >= 41 ){ echo '...';};
					echo '<br>';
				}
				if (!empty($txt_time)){
					echo '&#12296;時 間&#12297; ';
					echo nl2br(get_post_meta($post->ID, 'txt_time', true));
					echo '<br>';
				}
				if (!empty($txt_area)){
					echo '&#12296;場 所&#12297; ';
					echo nl2br(get_post_meta($post->ID, 'txt_area', true));
				}
				?>
				</div>
				<p class="btn"><a href="<?php echo $parm; ?>">詳細を見る</a></p>
		</li>
		<?php endwhile; endif; wp_reset_query(); ?>
	</ul>
	<p class="linkBtn"><a href="<?php bloginfo('url'); ?>/eventdetail/">一覧はこちら</a></p>
	
	<h3><img src="<?php bloginfo('template_url'); ?>/images/event/title02.png" alt="年間イベント情報"></h3>
	<ul class="event_list cf">
		<?php
		$seasonlist = array('ico01','ico02','ico03','ico04');
		$event_cnt = 0;
		foreach ($seasonlist as $season){
			if ($season == 'ico01'){
				//春
				$eventllist = array(104,113,114);
				$season_ico = 'haru';
			}
			if ($season == 'ico02'){
				//夏
				$eventllist = array(116,118,119);
				$season_ico = 'natu';
			}
			if ($season == 'ico03'){
				//秋
				$eventllist = array(120,121,122,123,124);
				$season_ico = 'aki';
			}
			if ($season == 'ico04'){
				//冬
				$eventllist = array(110,125);
				$season_ico = 'fuyu';
			}
			foreach ($eventllist as $event){
				$posts = get_posts(array("include"=>$event, "post_type"=>'event')); global $post;
				if($posts): foreach($posts as $post): setup_postdata($post); $id = $post->ID;
					echo '<li>';
					$image_id = SCF::get('img_main');
					$image = wp_get_attachment_image_src($image_id, 'full');	
					echo '<p class="thumb haru"><span><img src="'.get_template_directory_uri().'/images/event/'.$season_ico.'.png" alt=""></span><img src="'.$image[0].'"></p>';
					echo '<h4>'.nl2br(get_post_meta($post->ID, 'txt_list', true)).'</h4>';
					echo '<p class="btn"><a href="'.get_the_permalink().'">詳細を見る</a></p>';
					echo '</li>';
				endforeach; endif; wp_reset_postdata();
				}
			}
		 ?>
	</ul>
	</div>
</section>

</div><!-- //#content -->

<?php get_footer(); ?>
