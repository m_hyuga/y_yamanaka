<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta name="keywords" content="石川,温泉,山中,観光,観光協会">
<meta name="description" content="山中温泉観光協会のサイトです。開湯1300年の歴史と豊かな自然、文人墨客が愛した情緒あふれる温泉地です。">
<?php wp_head(); ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/common.css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/top.css">
<script src="<?php bloginfo('template_url'); ?>/common/js/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/common.js"></script>
<!--[if lt IE 9]>
<script src="<?php bloginfo('template_url'); ?>/common/js/html5shiv.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/css3-mediaqueries.js"></script>
<![endif]-->
<!-- <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/scrolltopcontrol.js"></script> -->

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/js/jquery.bxslider/jquery.bxslider.css">
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/skrollr-master/skrollr.min.js"></script>
<script type="text/javascript">
//スクロールゆらゆら定義
jQuery( function() {
	jQuery.fn.yurayura = function( config ){
		var obj = this;
		var i = 0;
		var defaults = {
			'move' : 5,			// 動く量
			'duration' : 1000,	// 移動にかける時間
			'delay' : 0			// 両端で停止する時間
		};
		var setting = jQuery.extend( defaults, config );
		( function move() {
			i = i > 0 ? -1 : 1;
			var p = obj.position().top;
			jQuery( obj )
				.delay( setting.delay )
				.animate( { top : p + i * setting.move }, {
					duration : setting.duration,
					complete : move
				});
		})();
	};
});

$(function(){
	//OP設定
	winHeight = $(window).height();  //ウィンドウの高さ  //グローバル化し、リサイズにも対応
	$('#op').height(winHeight);
	$('#op_slide').height(winHeight);
	$('#op_slide li').height(winHeight);
	$('.bx-viewport').height(winHeight);
	//以下 オープニングスライド
	$(window).load(function(){
		$('#btn_scrl').delay(300).animate({'opacity':'1'}, 3000);
		$('.op_bottom,#op_slide').delay(300).fadeIn(3000, function(){
			$('.op_logo').delay(100).fadeIn(3000);
			//パララックス実行
			var s = skrollr.init();
		});
	});
	setTimeout(function(){
		opSlider();
	}, 5000);
	function opSlider() {
		$('#op_slide').bxSlider({
			mode: 'fade', //フェードで切替
			auto: true, //自動で切替
			speed: 2500, //移り変わるスピード
			pause: 4500, //スライドを見せる時間
			pager: false, //ページャー消す
			controls: false, //コントロール消す
			infiniteLoop: true //ループ無し
		});
	}
	//リサイズ時にも高さ調整
	$(window).resize(function(){
		//OP設定
		winHeight = $(window).height();  //ウィンドウの高さ
		$('#op').height(winHeight);
		$('#op_slide li').height(winHeight);
		$('.bx-viewport').height(winHeight);
	});

	//pageTopボタン
	$(window).scroll(function(){
		if($(window).scrollTop() >= winHeight) {
			$('.slideWrap').css({'margin-top':'70px'});
			$('#topcontrol').fadeIn();
		} else {
			$('.slideWrap').css({'margin-top':'0'});
			$('#topcontrol').fadeOut();
		}
	});
	$('#topcontrol').click(function(){
		$('body, html').animate({ scrollTop: 0 }, 500);
	});

	//通常スライド
	// var slider = $('#slides').bxSlider({
	// 	auto: true,
	// 	speed: 1500, //移り変わるスピード
	// 	pause: 5500, //スライドを見せる時間
	// 	pager: true,
	// 	onSlideBefore: function(){
	// 		slider.startAuto();
	// 	},
	// 	onSlideAfter: function(){
	// 		slider.startAuto();
	// 	}
	// });
		var slider = $('#slides').bxSlider({
			auto: true,
			slideWidth: 980,
			speed: 1500, //移り変わるスピード
			pause: 5500, //スライドを見せる時間
			pager: false,
			minSlides: 3,
			maxSlides: 3,
			moveSlides: 1,
			slideMargin: 0,
			onSlideBefore: function(){
				slider.startAuto();
			},
			onSlideAfter: function(){
				slider.startAuto();
			}
		});

	//ループバナー
	$('.loopslider').bxSlider({
		auto: true,
		autoStart: true,
		infiniteLoop: true,
		moveSlides: 3,
		slideWidth: 317,
		randomStart: false,
		minSlides: 1,
		maxSlides: 5,
		slideMargin: 15,
		speed: 2500,
		pause: 5000,
		pager: false,
		controls: false
	});

	//カルーセル
	var car2 = $('#pla_cal').bxSlider({
		ticker: true,
		slideWidth: 250,
		speed: 70000, //移り変わるスピード
		pager: false,
		controls: false,
		slideMargin: 15
	});


	//メニュー追従
	$(window).scroll(function(){
		if($(window).scrollTop() >= winHeight) {
			$('#header').css({
				'position':'fixed',
				'top':0,
				'left':0
			});
		} else {
			$('#header').css({
				'position':'relative'
			});
		}
	});
	//ゆらゆら実行
	$('#btn_scrl').yurayura({
		'move' : 20, //動く量
		'delay' : 10, //両端で停止する時間
		'duration' : 600 //移動にかける時間
	});
	//スクロールボタン
	$('#btn_scrl').click(function(){
		$('body, html').animate({ scrollTop: winHeight }, 500);
	});

	//魅力のオーバ処理
	$('.conceptTop .bnr li').hover(function(){
		$(this).children('img').stop(true,false).animate({
			'width':545,
			'height':252,
			'top':-16,
			'left':-35
		}, 250);
	}, function(){
		$(this).children('img').stop(true,false).animate({
			'width':475,
			'height':220,
			'top':0,
			'left':0
		}, 250);
	});
	//見どころのオーバ処理
	// $('#pla_cal li a').hover(function(){
	// 	$(this).children('img').stop(true,false).animate({
	// 		'width':280,
	// 		'margin-left':-140,
	// 		'top': 0
	// 	}, 250);
	// }, function(){
	// 	$(this).children('img').stop(true,false).animate({
	// 		'width':250,
	// 		'margin-left':-125,
	// 		'top': 15
	// 	}, 250);
	// });


});
</script>
</head>

<body id="topIndex">
<div id="wrapper">
<div id="op">
	<div id="op_header">
		<div class="inner">
			<dl class="hnavi">
				<dt><a href="http://www.yamanaka-spa.or.jp/global/eng/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/english_off.png" width="58" height="20" alt="ENGLISH"></a></dt>
				<dd><a href="https://www.facebook.com/pages/%E5%B1%B1%E4%B8%AD%E6%B8%A9%E6%B3%89%E8%A6%B3%E5%85%89%E5%8D%94%E4%BC%9A/251607508211337" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/btn_facebook_off.png" width="24" height="24" alt="facebook"></a></dd>
				<dd><a href="#" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/btn_twitter_off.png" width="23" height="24" alt="twiiter"></a></dd>
			</dl>
			<ul id="op_navi">
				<li><a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn01_off.png" width="35" height="20" alt="ホーム"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/concept/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn02_off.png" width="55" height="20" alt="コンセプト"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/about/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn03_off.png" width="95" height="20" alt="山中温泉について"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/hotel/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn04_off.png" width="53" height="20" alt="旅館紹介"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/highlights/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn05_off.png" width="45" height="20" alt="見どころ"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/event/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn06_off.png" width="70" height="20" alt="イベント情報"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/access/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn07_off.png" width="68" height="20" alt="アクセス情報"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/form/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn08_off.png" width="72" height="20" alt="お問い合わせ"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/link/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn09_off.png" width="45" height="20" alt="リンク集"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/download/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn10_off.png" width="132" height="20" alt="パンフレットダウンロード"></a></li>
			</ul>
		</div>
	</div>
	<div id="btn_scrl"><img src="<?php bloginfo('template_url'); ?>/images/top/btn_scrl.png" alt="スクロール"></div>
	<div class="op_bottom"><img src="<?php bloginfo('template_url'); ?>/images/top/op/op_bottom.png" width="100%" alt=""></div>
	<div class="op_logo"><img src="<?php bloginfo('template_url'); ?>/images/top/op/slide_img.jpg" width="300" height="430" alt="日本の心に、つかろう。"></div>
		<ul id="op_slide" data-top-top="top:0%;" data-top-bottom="top:20%;">
			<li class="slide1"></li>
			<li class="slide2"></li>
			<li class="slide3"></li>
			<li class="slide4"></li>
		</ul>
</div>

<header id="header">
	<div class="inner">
		<h1 id="logo"><img src="<?php bloginfo('template_url'); ?>/common/images/logo.png" width="122" height="42" alt="日本の美と、渓谷の温泉と。山中温泉"></h1>
		<dl class="hnavi">
			<dt><a href="http://www.yamanaka-spa.or.jp/global/eng/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/english_off.png" width="58" height="20" alt="ENGLISH"></a></dt>
			<dd><a href="https://www.facebook.com/pages/%E5%B1%B1%E4%B8%AD%E6%B8%A9%E6%B3%89%E8%A6%B3%E5%85%89%E5%8D%94%E4%BC%9A/251607508211337" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/btn_facebook_off.png" width="24" height="24" alt="facebook"></a></dd>
			<dd><a href="#" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/btn_twitter_off.png" width="23" height="24" alt="twiiter"></a></dd>
		</dl>
		<ul id="navi">
			<li><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn01_on.png" width="35" height="20" alt="ホーム"></a></li>
			<li><a href="<?php bloginfo('url'); ?>/concept/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn02_off.png" width="55" height="20" alt="コンセプト"></a></li>
			<li><a href="<?php bloginfo('url'); ?>/about/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn03_off.png" width="95" height="20" alt="山中温泉について"></a></li>
			<li><a href=<?php bloginfo('url'); ?>/"hotel/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn04_off.png" width="53" height="20" alt="旅館紹介"></a></li>
			<li><a href="<?php bloginfo('url'); ?>/highlights/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn05_off.png" width="45" height="20" alt="見どころ"></a></li>
			<li><a href="<?php bloginfo('url'); ?>/event/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn06_off.png" width="70" height="20" alt="イベント情報"></a></li>
			<li><a href="<?php bloginfo('url'); ?>/access/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn07_off.png" width="68" height="20" alt="アクセス情報"></a></li>
			<li><a href="<?php bloginfo('url'); ?>/form/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn08_off.png" width="72" height="20" alt="お問い合わせ"></a></li>
			<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn09_off.png" width="45" height="20" alt="リンク集"></a></li>
			<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn10_off.png" width="132" height="20" alt="パンフレットダウンロード"></a></li>
		</ul>
	</div>
</header><!-- //#header -->

<div class="flexslider">
	<ul id="slides">
	<?php
	if (have_posts()) : 
	while (have_posts()) : the_post();
	$repeat_group = scf::get('slide_area');
	foreach ( $repeat_group as $field_name => $field_value ) :
		$txt_link = $field_value['txt_link'];
		$val =  $field_value["slide_img"];
		echo '<li>';
		if (!empty($txt_link)) { echo '<a href="'.$txt_link.'">';}
		$image = wp_get_attachment_image_src($val, 'full');
		echo '<img src="'.$image[0].'">';
		if (!empty($txt_link)) { echo '</a>';}
		echo '</li>';
		
	 endforeach;
	 endwhile; endif; ?>
	</ul>
	<!-- <a href="#" class="prev"><img src="<?php bloginfo('template_url'); ?>/common/images/arrow_left.png" width="22" height="40" alt="prev"></a> -->
	<!-- <a href="#" class="next"><img src="<?php bloginfo('template_url'); ?>/common/images/arrow_right.png" width="22" height="40" alt="next"></a> -->
</div>

<div id="contents">

	<section class="conceptTop">
		<div class="inner">
			<h2 class="alignCenter"><img src="<?php bloginfo('template_url'); ?>/images/top/ttl01.png" width="160" height="20" alt="コンテンツ CONTENTS"></h2>
			<p class="fo14 txt">開湯1300年、山中温泉は豊かな自然と伝統文化が残る渓谷の温泉地です。<br>松尾芭蕉も愛してやまなかった美しい湯のまちで｢日本の心｣につかりませんか。</p>
			<ul class="bnr">
				<li><a href="<?php bloginfo('url'); ?>/concept/" class="al"><img src="<?php bloginfo('template_url'); ?>/images/top/bnr01.jpg" width="276" height="276" alt="コンセプト CONCEPT"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/about/" class="al"><img src="<?php bloginfo('template_url'); ?>/images/top/bnr02.jpg" width="276" height="276" alt="山中温泉について ABOUT YAMANAKA"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/hotel/" class="al"><img src="<?php bloginfo('template_url'); ?>/images/top/bnr03.jpg" width="276" height="276" alt="旅館紹介 HOTEL INTRODUCTION"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/highlights/" class="al"><img src="<?php bloginfo('template_url'); ?>/images/top/bnr04.jpg" width="276" height="276" alt="見どころ HIGHLIGHTS"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/event/" class="al"><img src="<?php bloginfo('template_url'); ?>/images/top/bnr05.jpg" width="276" height="276" alt="イベント情報 EVENT INFORMATION"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/access/" class="al"><img src="<?php bloginfo('template_url'); ?>/images/top/bnr06.jpg" width="276" height="276" alt="アクセス情報 ACCESS INFORMATION"></a></li>
			</ul>
		</div>
	</section>

	<section class="newsTop">
		<div class="inner">
			<h2 class="alignCenter pb25"><img src="<?php bloginfo('template_url'); ?>/images/top/ttl02.png" width="112" height="20" alt="ニュース NEWS"></h2>
			<div class="news">
				<ul class="fo14">
					<?php
					$news_cnt = 0;
					query_posts(
						array(
						'post_type' => 'news',
						'posts_per_page' => 3
						 ) 
					);
					if (have_posts()) : while (have_posts()) : the_post(); 
					$news_cnt++;
					?>
					<li>
						<p class="ico"><?php $terms = get_the_terms( get_the_ID(), 'news_cat' );
																		if ( !empty($terms) ) {
																			if ( !is_wp_error( $terms ) ) {
																				foreach( $terms as $term ) {
																					echo '<span>'.$term->name.'</span>';
																						if ($term !== end($terms)) {
																							echo ' ';
																						}
																				}
																			}
																		} ?></p>
						<p class="dates"><?php the_time('Y.m.d'); ?></p>
						<p class="txt"><a href="<?php bloginfo('url'); ?>/news#news_0<?php echo $news_cnt; ?>"><?php if(mb_strlen($post->post_title)>30) { $title= mb_substr($post->post_title,0,30) ; echo $title. ･･･ ;} else {echo $post->post_title;}?></a></p>
					</li>
					<?php endwhile; endif; ?>
				</ul>
			</div>
			<p class="linkBtn fo11"><a href="<?php bloginfo('url'); ?>/news/">一覧はこちら</a></p>
		</div>
	</section>

</div><!-- //#content -->

<footer id="footer">
	<div id="finner">
		<ul class="fo11">
			<li><a href="<?php bloginfo('url'); ?>/concept/">コンセプト</a>|</li>
			<li><a href="<?php bloginfo('url'); ?>/about/">山中温泉について</a>|</li>
			<li><a href="<?php bloginfo('url'); ?>/hotel/">旅館紹介</a>|</li>
			<li><a href="<?php bloginfo('url'); ?>/form/">お問い合わせ・協会について</a>|</li>
			<li><a href="<?php bloginfo('url'); ?>/link/">リンク集</a>|</li>
			<li><a href="<?php bloginfo('url'); ?>/download/">パンフレットダウンロード</a>|</li>
		</ul>
		<p class="copyright"><small><img src="<?php bloginfo('template_url'); ?>/common/images/copyright.png" width="294" height="12" alt="Copyright&copy;Yamanakaonsen All Right Reserved."></small></p>
	</div>
</footer>
</div><!-- #wrapper -->
<div id="topcontrol">
<div id="scrolltopcontrol"></div>
</div>
<?php wp_footer(); ?>
</body>
</html>
