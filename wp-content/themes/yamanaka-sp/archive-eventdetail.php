<?php get_header(); ?>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/scrolltopcontrol.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/jquery.tile.min.js"></script>
<script>
$(window).on('load resize', function() {
    var columns = 2;
    var liLen = $('.new_event li').length;
    var lineLen = Math.ceil(liLen / columns);
    var liHiArr = [];
    var lineHiArr = [];

    for (i = 0; i <= liLen; i++) {
        liHiArr[i] = $('.new_event li').eq(i).height();
        if (lineHiArr.length <= Math.floor(i / columns) 
            || lineHiArr[Math.floor(i / columns)] < liHiArr[i]) 
        {
            lineHiArr[Math.floor(i / columns)] = liHiArr[i];
        }
    }
    $('.new_event li').each(function(i){
        $(this).css('height',lineHiArr[Math.floor(i / columns)] + 'px');
    });
});
</script>
<section class="mainimg">
<h1 class="headTitle"><img src="<?php bloginfo('template_url'); ?>/images/event/ttl.png" width="98" height="42" alt="イベント EVENT"></h1>
</section>

<div id="contents">
<ul class="path">
<li><a href="<?php bloginfo('url'); ?>">ホーム</a>&#65310;</li>
<li><a href="<?php bloginfo('url'); ?>/event/">イベント情報</a>&#65310;</li>
<li>新着イベント情報一覧</li>
</ul>

<section>
	<div class="inner eventArea">
	<div class="eventList clearfix">
		<h2 class="pb30"><img src="<?php bloginfo('template_url'); ?>/images/event/list/title_list.png"  alt="新着イベント情報一覧"></h2>
	<ul class="new_event cf">
		<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
		<li>
			<?php 
			$args = array(
							'connected_type' => 'posts_to_events',
							'connected_items' =>  $post->ID,
							'nopaging' => true,
							'suppress_filters' => false
			);
			$connected_posts = get_posts( $args );
			foreach ( $connected_posts as $post ) {
			// タイトル
			$title = get_the_title();
			echo '<h4>'.$title.'</h4>';
			setup_postdata( $post ); 
			// 画像
			$image_id = SCF::get('img_main');
			$image = wp_get_attachment_image_src($image_id, 'full');
			if (!empty($image_id)) {
				echo '<p class="thumb"><img src="'.$image[0].'" alt=""></p>';
			};
			
			
			// リンク
			$parm = get_permalink();

			} wp_reset_postdata();?>
			<div class="ex_txt">
				<?php
				$txt_date =  scf::get('txt_date');
				$txt_time =  scf::get('txt_time');
				$txt_area =  scf::get('txt_area');
				
				if (!empty($txt_date)) {
					echo '&#12296;日 時&#12297; ';
					$date_ver = strip_tags(get_post_meta($post->ID, 'txt_date', true));
					echo mb_substr($date_ver,0,40);
					if ( mb_strlen($date_ver) >= 41 ){ echo '...';};
					echo '<br>';
				}
				if (!empty($txt_time)){
					echo '&#12296;時 間&#12297; ';
					echo nl2br(get_post_meta($post->ID, 'txt_time', true));
					echo '<br>';
				}
				if (!empty($txt_area)){
					echo '&#12296;場 所&#12297; ';
					echo nl2br(get_post_meta($post->ID, 'txt_area', true));
				}
				?>
				</div>
				<p class="btn"><a href="<?php echo $parm; ?>">詳細を見る</a></p>
		</li>
		<?php endwhile; endif; wp_reset_query(); ?>
	</ul>
		
	</div>
		<ul class="newspager">
			<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
		</ul>
</div>
</section>

</div><!-- //#content -->
<?php get_footer(); ?>
